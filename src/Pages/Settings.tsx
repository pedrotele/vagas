import { Add, Apps, Cancel, Close, Lock, LockOpen, Notifications, WhereToVote } from "@material-ui/icons";
import React, { ChangeEvent, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Div from "../Components/Basics/Div/Div";
import Image from "../Components/Basics/Image/Image";
import Text from "../Components/Basics/Text/Text";
import Button from "../Components/Forms/Button/Button";
import Input from "../Components/Forms/Input/Input";
import ListaAtividades from "../Components/Pattern/ListaAtividades/ListaAtividades/ListaAtividades";
import Menu from "../Components/Pattern/Menu/Menu/Menu";
import Setor from "../Objects/Primarios/Setor";

const Settings = ()=>{

    const [stateMenuLateral,setStateMenuLateral]=useState(0);
    const [widthMenuLateral,setWidthMenuLateral]=useState("60px");
    const [displayType, setDisplayType]=useState("infosPess");
    const [atividades, setAtividades]=useState(new Array);

    const [token,setToken]=useState(getCookies()!.substring(1,getCookies()?.length!-1));
    const [user, setUser]=useState(parseJwt(token));


    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }
    
    function parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };

    useEffect(()=>{
        buscarLogs();
    },[]);

    useEffect(()=>{
        if(widthMenuLateral==="60px"){
          setStateMenuLateral(0);
        }else{
          setStateMenuLateral(1);
        }
    },[widthMenuLateral]);

    async function buscarLogs(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token}
            };
            await fetch("http://10.1.222.20:3001/logs", requestOptions)
                .then(res=>res.json())
                .then(res => {
                    if(res){
                        setAtividades(res);
                    }
                });
    }

    function deslogar(){
        document.cookie = "token" + "=" +";expires=Thu, 01 Jan 1970 00:00:01 GMT";
        window.location.href="http://localhost:3000/auth";
    }
        

    return(
        <Div width="100%" height="100%">
            <Div width="100%" display="flex" height="100vh" flexFlow="column">

                <Div width="100%" display="flex" height="100%" flexBasis="auto" flexFlow="1" background="#fcfcfc" >
                    
                    <Div  marginLeft="50px" flexGrow="1" height="100%" marginRight="20%" display="flex">
                            <Div width="100%" height="100%" display="block" overflow="scroll">
                                <Text width="100%" fontSize="70%" text="Página atual:" margin="30px" marginBottom="5px" marginLeft="70px"/>
                                <Text width="100%" fontSize="150%" text="Configurações" margin="30px" marginTop="0px" marginLeft="70px"/>
                                <Link to="/changepassword" style={{color:"#000", textDecoration:"none"}}>
                                    <Div  padding="5px" paddingLeft="15px" height="40px" display="flex" width="90%" margin="0 auto">
                                        <Div width="20px" height="20px" position="relative" top="50%" transform="translate(0, -50%)" >
                                            <LockOpen style={{height:"20px"}} htmlColor="#292929"/>
                                        </Div>
                                        <Text marginLeft="4%" position="relative" top="50%" transform="translate(0, -50%)" text="Alterar Senha" width="70%" height="20px" fontSize="95%" />
                                    </Div>
                                </Link>
                                <Div onClick={()=>{deslogar()}} paddingLeft="15px" padding="5px" height="40px" display="flex" width="90%" margin="0 auto">
                                    <Div width="20px" height="20px" position="relative" top="50%" transform="translate(0, -50%)" >
                                        <Close style={{height:"20px"}} htmlColor="#292929"/>
                                    </Div>
                                    <Text marginLeft="4%" position="relative" top="50%" transform="translate(0, -50%)" text="Sair da conta atual" width="70%" height="20px" fontSize="95%" />
                                </Div>



                            </Div>
                    </Div>

                    


                    <Div display="block" background="white" width="20%" height="100%" position="fixed" marginLeft="80%" borderLeft="1px solid #eeeeee">

                        <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                            <Text text="Minha Equipe" fontSize="100%" margin="15px" marginTop="10px"/>

                        </Div>

                        <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                            <Text text="Atividades" fontSize="100%" margin="15px" marginBottom="5px" marginTop="10px"/>
                            <ListaAtividades atividades={atividades}/>

                        </Div>

                    </Div>

                    <Menu position="fixed" state={stateMenuLateral} onMouseEnter={()=>{setWidthMenuLateral("150px")}} onMouseLeave={()=>{setWidthMenuLateral("60px")}} width={widthMenuLateral} height="100%" background="#ffffff" borderRight="1px solid #cccccc"/>                    

                </Div>
            </Div>

        </Div>
    );
}

export default Settings;