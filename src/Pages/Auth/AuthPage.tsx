import React, { useEffect, useState } from "react";
import Div from "../../Components/Basics/Div/Div";
import Image from "../../Components/Basics/Image/Image";
import Text from "../../Components/Basics/Text/Text";
import Button from "../../Components/Forms/Button/Button";
import Input from "../../Components/Forms/Input/Input";
import Card from "../../Components/Others/Card/Card";

const AuthPage = (props:any)=>{

    const [emailLogin, setEmailLogin]=useState("");
    const [senhaLogin, setSenhaLogin]=useState("");
    const [erro, setErro]=useState("");

        async function solicitarLogin(){
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json'},
                body: JSON.stringify({username:emailLogin, password:senhaLogin}),
            };
            await fetch("http://10.1.222.20:3001/auth", requestOptions)
                    .then(res=>res.json())
                    .then(res => {
                        if(res.access_token!==undefined){
                            var d = new Date();
                            d.setTime(d.getTime() + (60 *60 * 1000));
                            document.cookie = "token="+JSON.stringify(res.access_token)+";"+ "expires= " + d.toUTCString() + "; path=/";
                            window.location.href="http://localhost:3000/vagas";
                        }else{
                            setErro("Acesso não autorizado, verifique suas credenciais");
                        }
            });
        }


    return(
        <Div width="100%" height="100vh" background="#f1f1f1">
            <Card width="60%" height="80vh" position="absolute" top="50%" transform="translate(-50%, -50%)" left="50%">
                <Div width="35%" height="100%" background="#292929" float="left" display="block">
                    <Image width="13vw" height="13vw" image={require("../../Res/Images/logo.png")} margin="0 auto" position="relative" top="50%" transform="translate(0, -50%)"/>
                </Div>
                <Div width="65%" height="100%" background="#ffffff" float="right" display="column" justifyContent="center" alignItens="center" flexDirection="row" position="relative">
                    <Div marginTop="10%" height="5%" width="30%" margin="0 auto" display="flex"/>
                    <Div width="65%" height="50%" margin="0 auto">

                        <Div width="85%" background="#ffffff"  position="relative" top="50%" transform="translate(0, -50%)" margin="0 auto">
                                <Input onChange={(e)=>{setEmailLogin(e.target.value)}} width="100%" margin="0 auto" padding="8px" placeholder="Email" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" marginTop="5%"/>
                                <Input onChange={(e)=>{setSenhaLogin(e.target.value)}} width="100%" margin="0 auto" padding="8px" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" placeholder="Senha" marginTop="5%"/>
                                <Text height="30px" text={erro} fontSize="70%" textFillColor="red" textAlign="center" marginTop="15px"/>
                        </Div>

                    </Div>
                    <Div width="65%" height="30%" margin="0 auto">
                        <Button onClick={()=>{solicitarLogin()}} width="85%" margin="0 auto" padding="10px" border="none" borderRadius="5px" boxShadow="2px 2px #cccccc" marginTop="5%" color="#ffffff" background="#292929">Entrar</Button>
                    </Div>
                </Div>
            </Card>
        </Div>
    );
}

export default AuthPage;