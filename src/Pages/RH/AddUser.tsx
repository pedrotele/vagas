import { Apps, CallOutlined, Face, FaceOutlined, Notifications, Send, SendOutlined, WhereToVote } from "@material-ui/icons";
import React, { ChangeEvent, useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import Div from "../../Components/Basics/Div/Div";
import Image from "../../Components/Basics/Image/Image";
import Text from "../../Components/Basics/Text/Text";
import Button from "../../Components/Forms/Button/Button";
import DropDown from "../../Components/Forms/DropDown/DropDown/DropDown";
import Input from "../../Components/Forms/Input/Input";
import MultiLineText from "../../Components/Forms/MultiLineText/MultilineText";
import Header from "../../Components/Pattern/Header/Header";
import ListaAtividades from "../../Components/Pattern/ListaAtividades/ListaAtividades/ListaAtividades";
import Menu from "../../Components/Pattern/Menu/Menu/Menu";
import AccountForms from "../../Components/RH/AccountForms/AccountForms";
import ListaContatos from "../../Components/RH/Contatos/ListaContatos/ListaContatos";


const AddUser=()=>{

    const [stateMenuLateral,setStateMenuLateral]=useState(0);
    const [widthMenuLateral,setWidthMenuLateral]=useState("60px");
    const [selectedImageURL, setSelectedImageURL]=useState(require("../../Res/Images/logo.png"));
    const [atividades, setAtividades]=useState(new Array);



    const [token,setToken]=useState(getCookies()!.substring(1,getCookies()?.length!-1));
    const [user, setUser]=useState(parseJwt(token));


    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }
    
    function parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };

    useEffect(()=>{
        buscarLogs();
    },[]);


    useEffect(()=>{
        if(widthMenuLateral==="60px"){
          setStateMenuLateral(0);
        }else{
          setStateMenuLateral(1);
        }
    },[widthMenuLateral]);


    async function buscarLogs(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token}
            };
            await fetch("http://10.1.222.20:3001/logs", requestOptions)
                .then(res=>res.json())
                .then(res => {
                    if(res){
                        setAtividades(res);
                    }
                });
    }

    return(
        <Div width="100%" height="100%">
            <Div width="100%" display="flex" height="100vh" flexFlow="column">

                <Div width="100%" display="flex" height="100%" flexBasis="auto" flexFlow="1" background="#fcfcfc">
                    
                    <AccountForms withPassword={true}/>


                    <Div display="block" background="white" width="20%" height="100%" position="fixed" marginLeft="80%" borderLeft="1px solid #eeeeee">

                        <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                            <Text text="Minha Equipe" fontSize="100%" margin="15px" marginTop="10px"/>

                        </Div>

                        <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                            <Text text="Atividades" fontSize="100%" margin="15px" marginBottom="5px" marginTop="10px"/>     
                            <ListaAtividades atividades={atividades}/>
                        </Div>

                    </Div>

                    <Menu position="fixed" state={stateMenuLateral} onMouseEnter={()=>{setWidthMenuLateral("150px")}} onMouseLeave={()=>{setWidthMenuLateral("60px")}} width={widthMenuLateral} height="100%" background="#ffffff" borderRight="1px solid #cccccc"/>                    

                </Div>
            </Div>
        </Div>
    );
}

export default AddUser;