import { Add, Apps, Close, Notifications, WhereToVote } from "@material-ui/icons";
import React, { ChangeEvent, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Div from "../../Components/Basics/Div/Div";
import Image from "../../Components/Basics/Image/Image";
import Text from "../../Components/Basics/Text/Text";
import Button from "../../Components/Forms/Button/Button";
import DropDown from "../../Components/Forms/DropDown/DropDown/DropDown";
import Input from "../../Components/Forms/Input/Input";
import MultiLineText from "../../Components/Forms/MultiLineText/MultilineText";
import Header from "../../Components/Pattern/Header/Header";
import ListaAtividades from "../../Components/Pattern/ListaAtividades/ListaAtividades/ListaAtividades";
import Menu from "../../Components/Pattern/Menu/Menu/Menu";
import ListaUsuarios from "../../Components/RH/Usuarios/ListaUsuarios/ListaUsuarios";
import Setor from "../../Objects/Primarios/Setor";

const Usuarios = ()=>{
    const [stateMenuLateral,setStateMenuLateral]=useState(0);
    const [widthMenuLateral,setWidthMenuLateral]=useState("60px");
    const [atividades, setAtividades]=useState(new Array);
    const [token,setToken]=useState(getCookies()!.substring(1,getCookies()?.length!-1));
    const [user, setUser]=useState(parseJwt(token));
    const [users, setUsers]=useState(new Array);
    const [usersTot, setUsersTot]=useState(new Array);


    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }

    useEffect(()=>{
        buscarLogs();
        buscarUsers();
    },[]);
    
    function parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };

    async function buscarUsers(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token}
            };
            await fetch("http://10.1.222.20:3001/persons", requestOptions)
                .then(res=>res.json())
                .then(res => {
                        const us=new Array();
                        for(let i=0;i<res.length;i++){
                            if(res[i].is_active){
                               us.push(res[i]);
                            }
                        }
                        setUsers(us);
                        setUsersTot(us);
                });
    }

    useEffect(()=>{
        if(widthMenuLateral==="60px"){
          setStateMenuLateral(0);
        }else{
          setStateMenuLateral(1);
        }
    },[widthMenuLateral]);

    async function buscarLogs(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token}
            };
            await fetch("http://10.1.222.20:3001/logs", requestOptions)
                .then(res=>res.json())
                .then(res => {
                    if(res){
                        setAtividades(res);
                    }
                });
    }

    function pesquisa(e:string){
        if(e==""){
            setUsers(usersTot);
        }else{
            var pesq=new Array();
            for(var i=0;i<usersTot.length;i++){
                if(usersTot[i].name.toLowerCase().includes(e.toLowerCase())){
                    pesq.push(usersTot[i]);
                }
            }
            setUsers(pesq);

        }
    }
        

    return(
        <Div width="100%" height="100%">
            <Div width="100%" display="flex" height="100vh" flexFlow="column">

                <Div width="100%" display="flex" height="100%" flexBasis="auto" flexFlow="1" background="#fcfcfc" >
                    
                    <Div  marginLeft="50px" flexGrow="1" height="100%" marginRight="20%" display="flex">
                            <Div width="100%" height="100%" display="block" overflow="scroll">
                                <Text width="100%" fontSize="70%" text="Página atual:" margin="30px" marginBottom="5px" marginLeft="70px"/>
                                <Text width="100%" fontSize="150%" text="Usuários" margin="30px" marginTop="0px" marginLeft="70px"/>
                                <Div width="90%" margin="0 auto" height="50px" display="flex" marginTop="40px">
                                    <Input onChange={(e:any)=>{pesquisa(e.target.value)}} background="white" marginLeft="1%" width="50%" height="20px" placeholder="Pesquisa" type="text" borderBottomFocus="1px solid #eeeeee" borderRadius="5px" border="1px solid #eeeeee" borderBottom="1px solid #eeeeee" padding="10px"/>
                                    <Div padding="10px"  height="25px" width="25px" marginLeft="50%">
                                        <Link to={"/adduser"}>
                                            <Div display="block">
                                                <Add/>
                                            </Div>
                                        </Link>
                                    </Div>
                                </Div>

                                <ListaUsuarios width="90%" margin="0 auto" usuarios={users}/>

                            </Div>
                    </Div>

                    


                    <Div display="block" background="white" width="20%" height="100%" position="fixed" marginLeft="80%" borderLeft="1px solid #eeeeee">

                        <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                            <Text text="Minha Equipe" fontSize="100%" margin="15px" marginTop="10px"/>

                        </Div>

                        <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                            <Text text="Atividades" fontSize="100%" margin="15px" marginBottom="5px" marginTop="10px"/>
                            <ListaAtividades atividades={atividades}/>

                        </Div>

                    </Div>

                    <Menu position="fixed" state={stateMenuLateral} onMouseEnter={()=>{setWidthMenuLateral("150px")}} onMouseLeave={()=>{setWidthMenuLateral("60px")}} width={widthMenuLateral} height="100%" background="#ffffff" borderRight="1px solid #cccccc"/>                    

                </Div>
            </Div>

        </Div>
    );
}

export default Usuarios;