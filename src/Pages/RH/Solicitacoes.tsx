import React, { useContext, useEffect, useState } from "react";
import { Add, Close, Widgets, List, Clear, Notifications, WhereToVote, Apps, Done } from "@material-ui/icons";
import Div from "../../Components/Basics/Div/Div";
import Text from "../../Components/Basics/Text/Text";
import Input from "../../Components/Forms/Input/Input";
import Menu from "../../Components/Pattern/Menu/Menu/Menu";
import ListaAtividades from "../../Components/Pattern/ListaAtividades/ListaAtividades/ListaAtividades";
import VagaFinal from "../../Objects/RH/Vagas/VagaFinalizada";
import ListaSolicitacoes from "../../Components/RH/Solicitacoes/ListaSolicitacoes/ListaSolicitacoes";
import Header from "../../Components/Pattern/Header/Header";

const Solicitacoes = ()=>{
    const [widthMenuLateral,setWidthMenuLateral]=useState("60px");
    const [displayAdd,setDisplayAdd]=useState("none");
    const [statusAdd, setStatusAdd]=useState(false);
    const [stateMenuLateral,setStateMenuLateral]=useState(0);
    const [displayType, setDisplayType]=useState("abertas");
    const [atividades, setAtividades]=useState(new Array);
    const [statusPerfil, setStatusPerfil]=useState(false);
    const [displayConf, setDisplayConf]=useState("none");
    const [contratarVaga, setContratarVaga]=useState<any>();
    const [vagas, setVagas]=useState(new Array);
    const [vagasTot, setVagasTot]=useState(new Array);


    useEffect(()=>{
        if(widthMenuLateral==="60px"){
          setStateMenuLateral(0);
        }else{
          setStateMenuLateral(1);
        }
      },[widthMenuLateral]);

    const [token,setToken]=useState(getCookies()!.substring(1,getCookies()?.length!-1));
    const [user, setUser]=useState(parseJwt(token));


    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }
    
    function parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };

    useEffect(()=>{
        vagas.sort((a:any,b:any)=>{
            const dataA=new Date(a.anoAber, a.mesAber, a.diaAber, a.horaAber,a.minAber);
            const dataB=new Date(b.anoAber, b.mesAber, b.diaAber, b.horaAber,b.minAber);
            if(dataA.getTime()>dataB.getTime()){
                return 1;
            }else if(dataA.getTime()<dataB.getTime()){
                return -1;
            }else{
                return 0;
            }
        });
    },[vagas]);

    useEffect(()=>{
        if(widthMenuLateral==="60px"){
          setStateMenuLateral(0);
        }else{
          setStateMenuLateral(1);
        }
    },[widthMenuLateral]);


    useEffect(()=>{
        buscarVagas();
        buscarLogs();
    },[]);

    async function buscarLogs(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token}
            };
            await fetch("http://10.1.222.20:3001/logs", requestOptions)
                .then(res=>res.json())
                .then(res => {
                    if(res){
                        setAtividades(res);
                    }
                });
    }
  
    async function buscarVagas(){
            const requestOptions = {
                method: 'GET',
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token}
                };
                await fetch("http://10.1.222.20:3001/vacancies", requestOptions)
                    .then(res=>res.json())
                    .then(res => {
                        if(res){
                            const vag=new Array();
                            for(let i=0;i<res.length;i++){
                                if((!res[i].approved)&&(res[i].end_at!==null)){
                                    vag.push(res[i]);
                                }
                            }
                            setVagas(vag);
                            setVagasTot(vag);
                        }
                    });
    }
    

    function pesquisa(e:string){
        if(e==""){
            setVagas(vagasTot);
        }else{
            var pesq=new Array();
            for(var i=0;i<vagasTot.length;i++){
                if(vagasTot[i].name.toLowerCase().includes(e.toLowerCase())){
                    pesq.push(vagasTot[i]);
                }
            }
            setVagas(pesq);
        }
    }


    return(
        <Div width="100%" height="100%">
            <Div width="100%" display="flex" height="100vh" flexFlow="column">

                <Div width="100%" display="flex" height="100%" flexBasis="auto" flexFlow="1" background="#fcfcfc">
                    <Div  marginLeft="50px" flexGrow="1" height="100%" marginRight="20%" display="flex">
                        <Div width="100%" height="100%" display="block" overflow="scroll">
                            <Text width="100%" fontSize="150%" text="Solicitações" margin="30px" marginLeft="70px"/>
                            <Div width="90%" margin="0 auto" height="50px" display="flex" marginTop="40px">
                                <Input onChange={(e:any)=>{pesquisa(e.target.value)}} background="white" width="50%" height="20px" placeholder="Pesquisa" type="text" borderBottomFocus="1px solid #eeeeee" borderRadius="5px" border="1px solid #eeeeee" borderBottom="1px solid #eeeeee" padding="10px"/>
                            </Div>
                            <ListaSolicitacoes solicitacoes={vagas} marginTop="30px" width="100%"/>
                        </Div>
                    </Div>

                    <Div display="block" background="white" width="20%" height="100%" position="fixed" marginLeft="80%" borderLeft="1px solid #eeeeee">

                        <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                            <Text text="Minha Equipe" fontSize="100%" margin="15px" marginTop="10px"/>

                        </Div>

                        <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                            <Text text="Atividades" fontSize="100%" margin="15px" marginBottom="5px" marginTop="10px"/>
                            <ListaAtividades atividades={atividades}/>

                        </Div>

                    </Div>

                    <Menu position="fixed" state={stateMenuLateral} onMouseEnter={()=>{setWidthMenuLateral("150px")}} onMouseLeave={()=>{setWidthMenuLateral("60px")}} width={widthMenuLateral} height="100%" background="#ffffff" borderRight="1px solid #cccccc"/>                    

                </Div>
            </Div>
        </Div>
    );
}

export default Solicitacoes;