import React, { useEffect, useState } from "react";
import Div from "../Components/Basics/Div/Div";
import Text from "../Components/Basics/Text/Text";
import Menu from "../Components/Pattern/Menu/Menu/Menu";

const Home = ()=>{

    const [widthMenuLateral,setWidthMenuLateral]=useState("60px");
    const [stateMenuLateral,setStateMenuLateral]=useState(0);

    useEffect(()=>{
        if(widthMenuLateral==="60px"){
          setStateMenuLateral(0);
        }else{
          setStateMenuLateral(1);
        }
      },[widthMenuLateral]);

    return(
        <Div width="100%" height="100vh" display="flex" background="#fcfcfc">
            

            <Div  marginLeft="50px" flexGrow="1" marginRight="20%" display="flex">
                
                <Div width="100%" display="block" overflow="scroll">
                    <Text width="100%" fontSize="150%" text="Relações Humanas" margin="30px" marginLeft="70px"/>
                </Div>

            </Div>


            <Div display="block" background="white" width="20%" height="100vh" position="fixed" marginLeft="80%" borderLeft="1px solid #eeeeee">

                <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                    <Text text="Minha Equipe" fontSize="100%" margin="15px" marginTop="10px"/>

                </Div>

                <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                    <Text text="Atividades" fontSize="100%" margin="15px" marginBottom="5px" marginTop="10px"/>
                </Div>

            </Div>

            <Menu position="fixed" state={stateMenuLateral} onMouseEnter={()=>{setWidthMenuLateral("150px")}} onMouseLeave={()=>{setWidthMenuLateral("60px")}} width={widthMenuLateral} height="100vh" background="#ffffff" borderRight="1px solid #cccccc"/>


        </Div>
    );
}

export default Home;