import { Add, Apps, Call, CallOutlined, Close, EmailOutlined, FaceOutlined, Notifications, WhereToVote } from "@material-ui/icons";
import React, { ChangeEvent, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { setEnvironmentData } from "worker_threads";
import Div from "../Components/Basics/Div/Div";
import Text from "../Components/Basics/Text/Text";
import Button from "../Components/Forms/Button/Button";
import DropDown from "../Components/Forms/DropDown/DropDown/DropDown";
import Input from "../Components/Forms/Input/Input";
import ListSelfActivities from "../Components/Pattern/ListaAtividades copy/ListSelfActivities/ListSelfActivities";
import ListaAtividades from "../Components/Pattern/ListaAtividades/ListaAtividades/ListaAtividades";
import Menu from "../Components/Pattern/Menu/Menu/Menu";
import AccountForms from "../Components/RH/AccountForms/AccountForms";
import ListaContatos from "../Components/RH/Contatos/ListaContatos/ListaContatos";
import Setor from "../Objects/Primarios/Setor";

const Perfil = ()=>{

    const [stateMenuLateral,setStateMenuLateral]=useState(0);
    const [widthMenuLateral,setWidthMenuLateral]=useState("60px");
    const [displayType, setDisplayType]=useState("atividades");
    const [atividades, setAtividades]=useState(new Array);

    const [token,setToken]=useState(getCookies()!.substring(1,getCookies()?.length!-1));
    const [userToken, setUserToken]=useState(parseJwt(token));
    const [user, setUser]=useState<any>({Log:[]});


    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }
    
    function parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
        return JSON.parse(jsonPayload);
    };

    async function buscarSelf(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token}
            };
            await fetch("http://10.1.222.20:3001/persons/self", requestOptions)
                .then(res=>res.json())
                .then(res => {
                    if(res){
                        setUser(res);
                    }
                });
    }

    async function buscarLogs(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token}
            };
            await fetch("http://10.1.222.20:3001/logs", requestOptions)
                .then(res=>res.json())
                .then(res => {
                    if(res){
                        setAtividades(res);
                    }
                });
    }

    useEffect(()=>{
        buscarSelf();
        buscarLogs();
    },[]);

    useEffect(()=>{
        if(widthMenuLateral==="60px"){
          setStateMenuLateral(0);
        }else{
          setStateMenuLateral(1);
        }
    },[widthMenuLateral]);
     

    return(
        <Div width="100%" height="100%">
            <Div width="100%" display="flex" height="100vh" flexFlow="column">

                <Div width="100%" display="flex" height="100%" flexBasis="auto" flexFlow="1" background="#fcfcfc" >
                    
                    <Div  marginLeft="50px" flexGrow="1" height="100%" marginRight="20%" display="flex">
                            <Div width="100%" height="100%" display="block" overflow="scroll">
                                <Text width="100%" fontSize="70%" text="Página atual:" margin="30px" marginBottom="5px" marginLeft="70px"/>
                                <Text width="100%" fontSize="150%" text="Meu Perfil" margin="30px" marginTop="0px" marginLeft="70px"/>
                                <Div width="90%" margin="0 auto" height="50px" display="flex" marginTop="40px">
                                    <Div alignItens="right" justifyContent="right" height="40px" width="90%" marginLeft="15%" display="flex">
                                            <Button onClick={()=>{setDisplayType("atividades")}} borderBottom={displayType=="atividades"?"2px solid #292929":"none"} height="40px">
                                                <Text text="Atividades" height="20px" fontSize="100%" textFillColor={displayType=="atividades"?"#292929":"#bbbbbb"}/>
                                            </Button>
                                            <Button onClick={()=>{setDisplayType("infosPess")}} marginLeft="10px"  height="40px" borderBottom={displayType=="infosPess"?"2px solid #292929":"none"}>
                                                <Text text="Informações Pessoais" height="20px" fontSize="100%" textFillColor={displayType=="infosPess"?"#292929":"#bbbbbb"}/>
                                            </Button>
                                    </Div>
                                </Div>
                                
                                <ListSelfActivities display={displayType==="atividades"?"block":"none"} atividades={user.Log}/>

                                <AccountForms display={displayType==="infosPess"?"block":"none"} withPassword={false}/>

                            </Div>
                    </Div>

                    <Div display="block" background="white" width="20%" height="100%" position="fixed" marginLeft="80%" borderLeft="1px solid #eeeeee">

                        <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                            <Text text="Minha Equipe" fontSize="100%" margin="15px" marginTop="10px"/>

                        </Div>

                        <Div width="100%" height="50%" overflow="scroll" borderBottom="1px solid #eeeeee" >

                            <Text text="Atividades" fontSize="100%" margin="15px" marginBottom="5px" marginTop="10px"/>
                            <ListaAtividades atividades={atividades}/>

                        </Div>

                    </Div>

                    <Menu position="fixed" state={stateMenuLateral} onMouseEnter={()=>{setWidthMenuLateral("150px")}} onMouseLeave={()=>{setWidthMenuLateral("60px")}} width={widthMenuLateral} height="100%" background="#ffffff" borderRight="1px solid #cccccc"/>                    

                </Div>
            </Div>

        </Div>
    );
}

export default Perfil;