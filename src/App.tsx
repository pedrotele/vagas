import React from 'react';
import { AuthProv } from './Objects/Auth/AuthProv';
import Routes from './Routes/Routes';

function App() {
  return (
    <div>
          <Routes/>
    </div>
  );
}

export default App;
