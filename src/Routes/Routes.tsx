import { Routes as Router, Route, BrowserRouter } from "react-router-dom";
import Home from "../Pages/Home";
import AuthRoute from "../Objects/Auth/AuthRoute";
import AuthPage from "../Pages/Auth/AuthPage";
import Vagas from "../Pages/RH/Vagas";
import Solicitacoes from "../Pages/RH/Solicitacoes";
import Usuarios from "../Pages/RH/Usuarios";
import AddUser from "../Pages/RH/AddUser";
import Perfil from "../Pages/Perfil";
import Settings from "../Pages/Settings";
import ChangePassword from "../Pages/ChangePassword";


const Routes =()=>{
    
    return(
        <BrowserRouter>
            <Router>
                <Route path="/" element={<AuthRoute is_public={false} is_admin={false}><Home/></AuthRoute>}/>
                <Route path="/auth" element={<AuthRoute is_public={true} is_admin={false}><AuthPage/></AuthRoute>}/>
                <Route path='/vagas' element={<AuthRoute is_public={false} is_admin={false}><Vagas/></AuthRoute>}/>
                <Route path='/solicitacoes' element={<AuthRoute is_public={false} is_admin={true}><Solicitacoes/></AuthRoute>}/>
                <Route path='/usuarios' element={<AuthRoute is_public={false} is_admin={true}><Usuarios/></AuthRoute>}/>
                <Route path='/adduser' element={<AuthRoute is_public={false} is_admin={true}><AddUser/></AuthRoute>}/>
                <Route path='/perfil' element={<AuthRoute is_public={false} is_admin={false}><Perfil/></AuthRoute>}/>
                <Route path='/settings' element={<AuthRoute is_public={false} is_admin={false}><Settings/></AuthRoute>}/>
                <Route path='/changepassword' element={<AuthRoute is_public={false} is_admin={false}><ChangePassword/></AuthRoute>}/>
            </Router>
        </BrowserRouter>
    );
}

export default Routes