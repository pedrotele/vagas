import { createGlobalStyle } from 'styled-components';

import afbattersea from './afbattersea.ttf';
import PTSans from './PTSans.ttf';
import SourceCode from './SourceCodePro.ttf';
import Exo from './Exo2.ttf';

export default createGlobalStyle`
    @font-face {
        font-family: 'ptsans';
        src: local('Font Name'), local('FontName'),
        url(${PTSans}) format('truetype');
        font-weight: 300;
        font-style: normal;
    }

    @font-face {
        font-family: 'afbattersea';
        src: local('Font Name'), local('FontName'),
        url(${afbattersea}) format('truetype');
        font-weight: 300;
        font-style: normal;
    }

    @font-face {
        font-family: 'sourcecode';
        src: local('Font Name'), local('FontName'),
        url(${SourceCode}) format('truetype');
        font-weight: 300;
        font-style: normal;
    }

    @font-face {
        font-family: 'exo';
        src: local('Font Name'), local('FontName'),
        url(${Exo}) format('truetype');
        font-weight: 300;
        font-style: normal;
    }
`;