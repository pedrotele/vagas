class Setor{
    nome:string;
    gerente?:string;
    funcoes?:any;
    imagem?:any;

    constructor(nome:string, gerente?:string, funcoes?:any,imagem?:any){
        this.nome=nome;
        this.gerente=gerente;
        this.funcoes=funcoes;
        this.imagem=imagem;
    }
}

export default Setor;