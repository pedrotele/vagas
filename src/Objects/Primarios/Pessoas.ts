class Pessoas{
    nome:string;
    cpf:string;
    status:string;
    id:string;
    imagem:string;
    celulares:Array<String>;
    email:Array<String>;
    constructor(nome:string, cpf:string, status:string,id:string, imagem:string, celulares:Array<String>,email:Array<String>){
        this.nome=nome;
        this.cpf=cpf;
        this.status=status;
        this.id=id;
        this.imagem=imagem;
        this.celulares=celulares;
        this.email=email;
    }
}

export default Pessoas;