class Funcao{
    nome:string;
    setor:string;
    rat?:boolean;
    fgts?:boolean;
    vinte?:boolean;
    funcionario?:string;
    quantidade:number
    constructor(nome:string, setor:string,  quantidade:number, rat?:boolean, fgts?:boolean, vinte?:boolean, funcionario?:string){
        this.nome=nome;
        this.setor=setor;
        this.rat=rat;
        this.fgts=fgts;
        this.quantidade=quantidade;
        this.vinte=vinte;
        this.funcionario=funcionario;
    }
}

export default Funcao;