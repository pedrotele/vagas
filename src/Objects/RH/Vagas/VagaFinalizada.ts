import Vaga from "./Vaga";

class VagaFinal{
    name:string;
    desc:string;
    creator:string;
    status:any;
    id:string;
    relev:any;
    diaAber:number;
    horaAber:number;
    minutoAber:number;
    mesAber:number;
    anoAber:number;
    diaPrazo:number;
    horaPrazo:number;
    minutoPrazo:number;
    mesPrazo:number;
    anoPrazo:number;
    diaFinal:number;
    horaFinal:number;
    minutoFinal:number;
    mesFinal:number;
    anoFinal:number;
    constructor(vaga:any,status:string,fecham:Date){
        this.name=vaga.name;
        this.desc=vaga.desc;
        this.status=status;
        this.id=vaga.id;
        this.creator=vaga.creator;
        this.diaAber=vaga.diaAber;
        this.horaAber=vaga.horaAber;
        this.minutoAber=vaga.minutoAber;
        this.mesAber=vaga.mesAber;
        this.anoAber=vaga.anoAber;
        this.diaPrazo=vaga.diaPrazo;
        this.horaPrazo=vaga.horaPrazo;
        this.minutoPrazo=vaga.minutoPrazo;
        this.mesPrazo=vaga.mesPrazo;
        this.anoPrazo=vaga.anoPrazo;
        this.diaFinal=fecham.getDate();
        this.horaFinal=fecham.getHours();
        this.minutoFinal=fecham.getMinutes();
        this.mesFinal=fecham.getMonth();
        this.anoFinal=fecham.getFullYear();
    }

}



export default VagaFinal;