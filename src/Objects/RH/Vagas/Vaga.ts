class Vaga{
    id?:string;
    name:string;
    desc:string;
    creator:string;
    status:any;
    relev:any;
    diaAber:number;
    horaAber:number;
    minutoAber:number;
    mesAber:number;
    anoAber:number;
    diaPrazo:number;
    horaPrazo:number;
    minutoPrazo:number;
    mesPrazo:number;
    anoPrazo:number;
    constructor(nome:string, desc:string, criador:string, inicio:Date, prazo:Date,id?:string){
        this.name=nome;
        this.desc=desc;
        this.creator=criador;
        this.id=id;
        this.diaAber=inicio.getDate();
        this.horaAber=inicio.getHours();
        this.minutoAber=inicio.getMinutes();
        this.mesAber=inicio.getMonth();
        this.anoAber=inicio.getFullYear();
        this.diaPrazo=prazo.getDate();
        this.horaPrazo=prazo.getHours();
        this.minutoPrazo=prazo.getMinutes();
        this.mesPrazo=prazo.getMonth();
        this.anoPrazo=prazo.getFullYear();
        this.status=this.calcStatus();
    }

    calcStatus(){
        const prazo=new Date(this.anoPrazo, this.mesPrazo, this.diaPrazo);
        const now=new Date();
        if(now>prazo){
            return "Atrasado";
        }else{
            return "No Prazo";
        }
    }
}



export default Vaga;