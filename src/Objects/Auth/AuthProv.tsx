import React, {createContext, useEffect, useState} from "react";
import { getAuth, onAuthStateChanged, User} from "firebase/auth";
import { initializeApp } from "firebase/app";


export const AuthContext=React.createContext(null);

export const AuthProv=(props:any)=>{

    const [waiting,setWaiting]=useState(true);
    const [token, setToken]=useState<any>(getCookies());
    
    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }


    async function solicitarLogin(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', "Authorization":token},
            body: JSON.stringify({username:"lincoln.chaves@teleguardian.com.br", password:"123456"}),
            };
            await fetch("http://10.1.222.20:3001/auth", requestOptions)
                .then(res=>res.json())
                .then(res => {
                    if(res.access_token){
                        setWaiting(false);
                    }else{

                    }
                });
                setWaiting(false);
    }

    useEffect(()=> {
        if(token!=null){
            solicitarLogin();
        }else{
            setWaiting(false);
        }
    },[]);

    if(waiting){
        return (<>Carregando...</>);
    }else{
            return (
                <AuthContext.Provider value={token}>{props.children}</AuthContext.Provider>
            );
    }
    
};