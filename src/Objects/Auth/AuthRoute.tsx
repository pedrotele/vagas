import React, { useContext, useState } from "react";
import { Route, Navigate } from "react-router-dom";
import { AuthContext } from "./AuthProv";

const AuthRoute=(props:any)=>{

    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }
    
    function parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };



    if(props.is_public===true){
        return props.children;
    }else{
            var token;
            var cookies=getCookies();
            if(cookies!==null){
                token=cookies.substring(1,cookies.length!-1);
            }else{
                return (<Navigate to={"/auth"}/>);
            }
            
            const user=parseJwt(token);
        if(props.is_admin===true){
            if(user.is_admin===true){
                return(user?(props.children):(<Navigate to={"/auth"}/>));
            }else{
                return(<Navigate to={"/vagas"}/>);//Sem permissão
            }
        }else{
            return(user?(props.children):(<Navigate to={"/auth"}/>));
        }
    }
}

export default AuthRoute;