class Atividade{
    nome:string;
    desc:string;
    criador:string;
    criacao:string;
    dia:number;
    hora:number;
    ano:number;
    minuto:number;
    mes:number;
    type:string;
    constructor(type:string, nome:string, desc:string, criador:string, criacao:string, inicio:Date){
        this.nome=nome;
        this.desc=desc;
        this.criador=criador;
        this.criacao=criacao;
        this.dia=inicio.getDate();
        this.hora=inicio.getHours();
        this.mes=inicio.getMonth();
        this.ano=inicio.getFullYear();
        this.minuto=inicio.getMinutes();
        this.type=type;
    }
}



export default Atividade;