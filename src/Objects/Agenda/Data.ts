class Data{
    ano:number;
    mes:number;
    dia:number;
    hora:number;
    minuto:number;
    atividades:any;
    constructor(ano:number, mes:number, dia:number,hora:number, minuto:number, atividades?:any){
        this.ano=ano;
        this.minuto=minuto;
        this.mes=mes;
        this.hora=hora;
        this.dia=dia;
        this.atividades=atividades;
    }
}

export default Data;