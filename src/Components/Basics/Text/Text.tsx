import React from "react";
import { TextComp } from "./TextComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    paddingTop?:string;
    fontSize?:string;
    fontFamily?:string;
    textAlign?:string;
    backgroundClip?:string;
    textFillColor?:string;
    lineHeight?:string;
    verticalAlign?:string;
    text?:string;
    textOverflow?:string;
    whiteSpace?:string;
}

const Text:React.FC<Props>=(props)=>{
    return(
        <TextComp { ...props}>
            {props.text}
        </TextComp>
    )
}

export default Text;