import React from "react";
import { InputComp } from "./InputComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    color?:string;
    fontSize?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    fontFamily?:string;
    textAlign?:string;
    opacity?:string;
    zindex?:string;
    borderBottomFocus?:string;
    onChange?: (e:any) => void;
    placeholder?:string;
    type?:string;
    value?:string;
    flexGrow?:string;
    pattern?:string;
}

const Input:React.FC<Props>=(props)=>{
    return(
        <InputComp placeholder={props.placeholder} onChange={props.onChange} {...props}>
            {props.children}
        </InputComp>
    )
}

export default Input;