import React from "react";
import { MultilineTextComp } from "./MultilineTextComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    color?:string;
    fontSize?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    fontFamily?:string;
    textAlign?:string;
    opacity?:string;
    zindex?:string;
    borderBottomFocus?:string;
    onChange?: (e:any) => void;
    placeholder?:string;
    type?:string;
    value?:string;
    flexGrow?:string;
    resize?:string;
}

const MultiLineText:React.FC<Props>=(props)=>{
    return(
        <MultilineTextComp placeholder={props.placeholder} onChange={props.onChange} {...props}>
            {props.children}
        </MultilineTextComp>
    )
}

export default MultiLineText;