import { ExpandLess, ExpandMore } from "@material-ui/icons";
import React, {useState} from "react";
import Div from "../../../Basics/Div/Div";
import Text from "../../../Basics/Text/Text";
import ItensDropDown from "../ItensDropDown/ItensDropDown";
import { DropDownComp } from "./DropDownComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    itens:any;
    selected:any;
    setSelected:any;
    state:string;
    setState:any;
}

const DropDown:React.FC<Props>=(props)=>{

    return(
        <DropDownComp {...props}>
            <Div height="25px" padding="10px" display="flex" borderBottom={props.state==="none"?"none":"1px solid #eeeeee"} onClick={()=>{ props.state==="none"?props.setState("block"):props.setState("none")}}>
                <Text text={props.selected===""?"Selecione um tipo":props.selected} width="96%" fontSize="80%" height="18px" position="relative" top="50%" transform="translate(0, -50%)"/>
                <Div width="30px" background="white">
                    {props.state==="none"?<ExpandMore/>:<ExpandLess/>}
                </Div>
            </Div>

            <Div width="100%" display={props.state} overflow="scroll">

                 {props.itens.map((nome:any, index:number)=>(<ItensDropDown key={index} nome={nome} background={nome===props.selected?"#cccccc":"white"} onClick={()=>{props.setSelected(nome)}}/>))}

            </Div>
        </DropDownComp>
    )
}

export default DropDown;