import React from "react";
import Text from "../../../Basics/Text/Text";
import { ItensDropDownComp } from "./ItensDropDownComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    nome:string;
}

const ItensDropDown:React.FC<Props>=(props)=>{
    return(
        <ItensDropDownComp borderBottom="1px solid #eeeeee" height="25px" padding="10px" width="100%" {...props}>
            <Text text={props.nome} width="270px" fontSize="80%" height="18px" position="relative" top="50%" transform="translate(0, -50%)"/>
        </ItensDropDownComp>
    )
}

export default ItensDropDown;