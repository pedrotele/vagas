import { Add } from "@material-ui/icons";
import React, { useState, memo, useEffect } from "react";
import Atividade from "../../../Objects/Agenda/Atividade";
import Data from "../../../Objects/Agenda/Data";
import Div from "../../Basics/Div/Div";
import Text from "../../Basics/Text/Text";
import Button from "../../Forms/Button/Button";
import DropDown from "../../Forms/DropDown/DropDown/DropDown";
import Input from "../../Forms/Input/Input";
import MultiLineText from "../../Forms/MultiLineText/MultilineText";
import Calendario from "../Calendario/Calendario";
import CelulaDiasAgenda from "../CelulaDiasAgenda/CelulaDiasAgenda";
import { ListaAtividadesComp } from "./ListaAtividadesComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
}

const ListaAtividades:React.FC<Props>=(props)=>{

    const [listData, setListData]=useState(new Array());
    const [atividades, setAtividades]=useState(new Array());
    const [stateDropdown, setStateDropDown]=useState("none");
    const [selectedType, setSelectedType]=useState("");
    const [nome, setNome]=useState("");
    const [desc, setDesc]=useState("");
    const [selectedDay, setSelectedDay]=useState(new Date());
    

    const types=new Array();
    types.push("Bases");
    types.push("Clientes");
    types.push("Propagandas");

    useEffect(()=>{
      calcCalendar();
    },[atividades]);
  


    function calcCalendar(){
        var now=new Date();
        var year=now.getFullYear();
        var month=now.getMonth();
        var day=now.getDate();
        var lastDayOfMonth=0;
        var list=new Array();
        while(list.length<100){
          lastDayOfMonth=new Date(year, month+1,0).getDate();
          const date=new Date(year, month,day);
          const data=new Data(year, month, day, date.getHours(), date.getMinutes(), new Array());
          for(var i=0;i<atividades.length;i++){
              if((atividades[i].ano===year)&&(atividades[i].mes===month)&&(atividades[i].dia===day)){
                data.atividades.push(atividades[i]);
              }
          }
          list.push(data);
          day++;
          if(day>lastDayOfMonth){
            month++;
            day=1;
            if(month==12){
              month=0;
              year++;
            }
          }
        }
        setListData(list);
      }

    const [leftGuia,setLeftGuia]=useState("0%");
    const [displayAdd,setDisplayAdd]=useState("none");
    const [heightGuia, setHeightGuia]=useState("0%");
    const [statusNotification, setStatusNotification]=useState(false);


    function adicionarNotification(e:any){
      if(!statusNotification){
        setDisplayAdd("flex");
        var element=document.getElementsByClassName("buttonAdd");
        var left=element[0].getBoundingClientRect().right;
        left-=300;
        setLeftGuia(left+"px");
        setStatusNotification(true);
      }else{
        setDisplayAdd("none");
        setStatusNotification(false);
      }
    }

    return(
        <ListaAtividadesComp id={props.id} {...props}>
            <Div width="100%" display="block">
              <Text width="100%" fontSize="150%" text="Agenda" margin="30px" marginLeft="40px"/>
              <Div width="100%" height="50px" display="flex" marginTop="30px">
                <Input width="50%" marginLeft="30px"  height="20px" placeholder="Pesquisa" type="text" borderBottomFocus="1px solid #eeeeee" borderRadius="5px" border="1px solid #eeeeee" borderBottom="1px solid #eeeeee" padding="10px"/>
                <Div className={"buttonAdd"}  padding="10px" height="25px" width="25px" background="white" marginLeft="35%" onClick={(e:any)=>{adicionarNotification(e)}}>
                   <Add/>
                </Div>
              </Div>
              <Div position="relative" display={displayAdd} height="300px" border="1px solid #eeeeee" borderRadius="5px" width="80%" float="right">
                  
                <Calendario padding="20px" selecionarDia={setSelectedDay} width="40%" height="90%" margin="0 auto"/>
                <Div background="#eeeeee" height="80%" position="relative" top="50%" transform="translate(0, -50%)" width="2px"/>
                <Div width="60%" display="block" padding="10px">  
                  <Input onChange={(e:any)=>{setNome(e.target.value)}} width="90%" margin="0 auto" padding="8px" placeholder="Nome" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" marginTop="10px"/>
                  <DropDown marginTop="10px" state={stateDropdown} setState={setStateDropDown} width="90%" margin="0 auto" selected={selectedType} setSelected={setSelectedType} itens={types}/>
                  <Input type="time" marginTop="10px" padding="10px" width="90%" margin="0 auto"/>
                  <MultiLineText value={desc} resize="none" onChange={(e)=>{setDesc(e.target.value)}} width="90%" height="50px" padding="10px" placeholder="Descrição da Marca" borderBottomFocus="none" margin="0 auto"/>
                  <Button onClick={()=>{}} background="#292929" width="80%" margin="0 auto" height="40px" borderRadius="10px" >
                    <Text text="Adicionar" height="20px" fontSize="100%" textFillColor="#f5f5f5"/>
                  </Button>
                </Div>
              </Div>
            </Div>
            {listData.map((item, index)=>(<CelulaDiasAgenda key={index} dia={item}/>))}

            
        </ListaAtividadesComp>
    )
}

export default memo(ListaAtividades);