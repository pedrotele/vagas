import React from "react";
import Div from "../../Basics/Div/Div";
import Text from "../../Basics/Text/Text";
import { LinhaCalendarioComp } from "./LinhaCalendarioComp";


interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    dias:any;
    selecionarDia:any;
}

const LinhaCalendario:React.FC<Props>=(props)=>{
    return(
        <LinhaCalendarioComp {...props}>
            {props.dias.map((item:any,index:number)=>(<Div cursor="pointer" key={index} onClick={()=>{props.selecionarDia(item.date)}}><Text  text={item.date.getDate()}  textFillColor={item.color} padding="10%" textAlign="center" paddingTop="20%" paddingBottom="20%"  fontSize="10pt"/></Div>))}
        </LinhaCalendarioComp>
    )
}

export default LinhaCalendario;