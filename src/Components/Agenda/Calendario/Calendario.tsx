import { clear } from "@testing-library/user-event/dist/clear";
import React, { useEffect, useState } from "react";
import LinhaCalendario from "../LinhaCalendario/LinhaCalendario";
import { CalendarioComp } from "./CalendarioComp";
import { ChevronLeft, ChevronRight } from "@material-ui/icons";
import Div from "../../Basics/Div/Div";
import Text from "../../Basics/Text/Text";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    selecionarDia:any;
}


const Calendario:React.FC<Props>=(props)=>{

    class Dia{
        color:string;
        date:Date;
        clicavel:boolean;
        constructor(){
            this.color="";
            this.date=new Date();
            this.clicavel=false;
        }
    }

    const segunda=new Array();
    const terca=new Array();
    const quarta=new Array();
    const quinta=new Array();
    const sexta=new Array();
    const sabado=new Array();
    const domingo=new Array();

    var now=new Date();
    const [year, setYear]=useState(now.getFullYear());
    const [month, setMonth]=useState(now.getMonth());
    const [day, setDay]=useState(now.getDate());

    distribuirCalendario();

    useEffect(()=>{
        distribuirCalendario()
    },[month]);

    

    function distribuirCalendario(){

        var lastDateMonthBehind=new Date(year, month,0);
        var lastDateThisMonth=new Date(year, month+1,0);
        var ultimoDom=new Date();
        var veriUltimoDom=280;
        var veriPrimeiroDom=270
        var primeiroDom=new Date();

        for(var j=0;veriUltimoDom===280;j--){
            var date=new Date(year, month,j);
            if(date.getDay()===0){
                ultimoDom=date;
                veriUltimoDom=10;
            }
        }

        for(var j=lastDateThisMonth.getDate();veriPrimeiroDom===270;j++){
            var date=new Date(year, month,j+1);
            if(date.getDay()===0){
                primeiroDom=date;
                veriPrimeiroDom=17;
            }
        }

        for(var i=ultimoDom.getDate();i<=lastDateMonthBehind.getDate();i++){
            var day=new Date(year,month-1,i);
            var dayObj=new Dia();
            dayObj.clicavel=false;
            dayObj.color="#e2e2e2";
            dayObj.date=day;
            if(day.getDay()===0){
                domingo.push(dayObj);
            }else if(day.getDay()===1){
                segunda.push(dayObj);
            }else if(day.getDay()===2){
                terca.push(dayObj);
            }else if(day.getDay()===3){
                quarta.push(dayObj);
            }else if(day.getDay()===4){
                quinta.push(dayObj);
            }else if(day.getDay()===5){
                sexta.push(dayObj);
            }else if(day.getDay()===6){
                sabado.push(dayObj);
            }
        }

        for(var i=1;i<=lastDateThisMonth.getDate();i++){
            var day=new Date(year,month,i);
            var dayObj=new Dia();
            dayObj.clicavel=true;
            dayObj.color="#000000";
            dayObj.date=day;
            if(day.getDay()===0){
                domingo.push(dayObj);
            }else if(day.getDay()===1){
                segunda.push(dayObj);
            }else if(day.getDay()===2){
                terca.push(dayObj);
            }else if(day.getDay()===3){
                quarta.push(dayObj);
            }else if(day.getDay()===4){
                quinta.push(dayObj);
            }else if(day.getDay()===5){
                sexta.push(dayObj);
            }else if(day.getDay()===6){
                sabado.push(dayObj);
            }
        }

        for(var i=1;i<primeiroDom.getDate();i++){
            var day=new Date(year,month+1,i);
            var dayObj=new Dia();
            dayObj.clicavel=false;
            dayObj.color="#e2e2e2";
            dayObj.date=day;
            if(day.getDay()===0){
                domingo.push(dayObj);
            }else if(day.getDay()===1){
                segunda.push(dayObj);
            }else if(day.getDay()===2){
                terca.push(dayObj);
            }else if(day.getDay()===3){
                quarta.push(dayObj);
            }else if(day.getDay()===4){
                quinta.push(dayObj);
            }else if(day.getDay()===5){
                sexta.push(dayObj);
            }else if(day.getDay()===6){
                sabado.push(dayObj);
            }
        }
    }

    function clear(array:any){
        for(var o=0;o<array.length;o++){
            array.pop();
        }
    }

    function voltarMes(){
        var newMonth=month-1;
        clear(segunda);
        clear(terca);
        clear(quarta);
        clear(quinta);
        clear(sexta);
        clear(sabado);
        clear(domingo);
        if(month===0){
            setMonth(11);
            setYear(year-1);
        }else{
            setMonth(newMonth);
        }
    }


    function passarMes(){
        var newMonth=month+1;
        clear(segunda);
        clear(terca);
        clear(quarta);
        clear(quinta);
        clear(sexta);
        clear(sabado);
        clear(domingo);
        if(month===11){
            setMonth(0);
            setYear(year+1);
        }else{
            setMonth(newMonth);
        } 

    }

    const nomeMes=["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

    return(
        <CalendarioComp {...props}>
            <Div width="100%" height="50px" borderBottom="1px solid #eeeeee" boxShadow="0px 1px 0px 0px #eeeeee" display="block">
                <Div height="40%" width="40%" float="left" display="flex" position="relative" top="50%" transform="translateY(-50%)" marginLeft="3%">
                    
                    <Text text={nomeMes[month]} marginLeft="7%" height="100%" width="100%" fontSize="13pt"/>
                </Div>

                <Div height="100%" width="50%" float="right" display="block">
                    <Div onClick={passarMes} float="right"  width="25%" height="100%" display="flex" alignItens="center" justifyContent="center">
                        <Div width="24px" height="24px" position="relative" top="50%" transform="translateY(-50%)">
                            <ChevronRight/>
                        </Div>
                    </Div>
                    <Div onClick={voltarMes}  float="right" width="25%" height="100%" display="flex" alignItens="center" justifyContent="center">
                        <Div width="24px" height="24px" position="relative" top="50%" transform="translateY(-50%)">
                            <ChevronLeft/>
                        </Div>
                    </Div>
                </Div>
            </Div>
            <Div width="100%" height="40px" display="flex" marginTop="4%">
                <Text text="DOM" width="14%" height="100%" padding="1%" textAlign="center" fontSize="10pt"/>
                <Text text="SEG" width="14%" height="100%" padding="1%" textAlign="center" fontSize="10pt"/>
                <Text text="TER" width="14%" height="100%" padding="1%" textAlign="center" fontSize="10pt"/>
                <Text text="QUA" width="14%" height="100%" padding="1%" textAlign="center" fontSize="10pt"/>
                <Text text="QUI" width="14%" height="100%" padding="1%" textAlign="center" fontSize="10pt"/>
                <Text text="SEX" width="14%" height="100%" padding="1%" textAlign="center" fontSize="10pt"/>
                <Text text="SAB" width="14%" height="100%" padding="1%" textAlign="center" fontSize="10pt"/>
            </Div>
            <Div width="100%" height="200px" paddingBottom="2%" display="flex">
                <LinhaCalendario selecionarDia={props.selecionarDia} width="14%" height="100%" dias={domingo}/>
                <LinhaCalendario selecionarDia={props.selecionarDia} width="14%" height="100%" dias={segunda}/>
                <LinhaCalendario selecionarDia={props.selecionarDia} width="14%" height="100%" dias={terca}/>
                <LinhaCalendario selecionarDia={props.selecionarDia} width="14%" height="100%" dias={quarta}/>
                <LinhaCalendario selecionarDia={props.selecionarDia} width="14%" height="100%" dias={quinta}/>
                <LinhaCalendario selecionarDia={props.selecionarDia} width="14%" height="100%" dias={sexta}/>
                <LinhaCalendario selecionarDia={props.selecionarDia} width="14%" height="100%" dias={sabado}/>
            </Div>
            <Div width="100%" height="2px" background="#eeeeee" margin="0 auto"/>
        </CalendarioComp>
    )
}


export default Calendario;