import React from "react";
import Div from "../../Basics/Div/Div";
import Text from "../../Basics/Text/Text";
import { CelulaAtividadeAgendaComp } from "./CelulaAtividadeAgendaComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    atividade:any;
}

const CelulaAtividadeAgenda:React.FC<Props>=(props)=>{
    return(
        <CelulaAtividadeAgendaComp width="80%" margin="0 auto" height="60px" display="flex">
            
            <Text text={props.atividade.hora+":"+props.atividade.minuto} height="20px" fontSize="100%" position="relative" top="50%" transform="translateY(-50%)"/>

            <Div background="red" width="2px" marginLeft="2%" marginRight="2%" height="65%" position="relative" top="50%" transform="translateY(-50%)"/>

            <Div height="45px"  position="relative" top="50%" width="90%" transform="translateY(-50%)">
                
                <Text text={props.atividade.name}  width="80%" fontSize="80%"/>
                <Text text={props.atividade.desc}  width="80%" fontSize="60%"/>
                
                

            </Div>

        </CelulaAtividadeAgendaComp>
    )
}

export default CelulaAtividadeAgenda;