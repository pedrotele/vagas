import { Add } from "@material-ui/icons";
import { listAll } from "firebase/storage";
import React from "react";
import CelulaAtividadeAgenda from "../CelulaAtividadeAgenda/CelulaAtividadeAgenda";
import { CelulaDiasAgendaComp } from "./CelulaDiasAgendaComp";
import Div from "../../Basics/Div/Div";
import Text from "../../Basics/Text/Text";
import Atividade from "../../../Objects/Agenda/Atividade";
import Data from "../../../Objects/Agenda/Data";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    dia:Data;
}

const CelulaDiasAgenda:React.FC<Props>=(props)=>{

    function getMonthName(mes:number){
        switch(mes){
            case 0:
                return "janeiro";
            case 1:
                return "fevereiro";
            case 2:
                return "março";
            case 3:
                return "abril";
            case 4:
                return "maio";
            case 5:
                return "junho";
            case 6:
                return "julho";
            case 7: 
                return "agosto";
            case 8:
                return "setembro";
            case 9:
                return "outubro";
            case 10:
                return "novembro";
            case 11: 
                return "dezembro";
            default:
                return "Erro";
        }
    }

    return(
        <CelulaDiasAgendaComp width="100%" display="block" margin="10px" borderBottom="1px solid #eeeeee">
            <Div width="90%" margin="0 auto" height="35px" display="flex" >
                <Text width="90%" marginLeft="5%" text={props.dia.dia.toString()+" de "+getMonthName(props.dia.mes)+" de "+props.dia.ano.toString()} fontSize="90%" position="relative" top="50%" transform="translateY(-50%)"/>
            </Div>
            {props.dia.atividades.map((item:Atividade,index:number)=>(<CelulaAtividadeAgenda key={index} atividade={item}/>))}

        </CelulaDiasAgendaComp>
    )
}

export default CelulaDiasAgenda;