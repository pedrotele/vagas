import React from "react";
import Div from "../../../Basics/Div/Div";
import Text from "../../../Basics/Text/Text";
import { ItemListaAtividadeComp } from "./ItemListaAtividadeComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    atividade:any;
}

const ItemListaAtividade:React.FC<Props>=(props)=>{
    const created=new Date(props.atividade.created_at);
        return(
            <ItemListaAtividadeComp borderBottom="1px solid #eeeeee" background="white" width="95%" display="block" margin="0 auto">
                    <Div display="flex" height="40px" marginTop="10px">
                        <Div marginLeft="20px" display="block" top="50%" float="left" transform="translate(0, -50%)" height="28px"   position="relative">
                            <Text text={props.atividade.person.name} whiteSpace="nowrap" textOverflow="ellipsis" fontSize="70%"  width="100%" overflow="hidden" />
                        </Div>
                    </Div>
                    <Div display="block">
                        <Text text={props.atividade.action==="finished"?("A vaga "+props.atividade.log.name+" foi finalizada."):("A vaga "+props.atividade.log.name+" foi aberta.")} marginLeft="10px" marginRight="10%" fontSize="60%"  width="80%" />
                        <Text text={created.getDate()+"/"+created.getMonth()+"/"+created.getFullYear()+" - "+created.getHours()+":"+created.getMinutes()} float="right" textAlign="right" fontSize="60%"  padding="10px"  width="980%" />
                    </Div>
            </ItemListaAtividadeComp>
        )
}

export default ItemListaAtividade;