import React from "react";
import VagaFinal from "../../../../Objects/RH/Vagas/VagaFinalizada";
import Div from "../../../Basics/Div/Div";
import Text from "../../../Basics/Text/Text";
import ItemListaAtividade from "../ItemListaAtividade/ItemListaAtividade";
import { ListaAtividadesComp } from "./ListaAtividadesComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    cursor?:any;
    className?:any;
    zindex?:string;
    atividades:any
}

const ListaAtividades:React.FC<Props>=(props)=>{
        return(
            <ListaAtividadesComp id={props.id} {...props}>
                {props.atividades.map((atividade:any, index:number)=>(<ItemListaAtividade key={index} atividade={atividade}/>))}
            </ListaAtividadesComp>
        )
}

export default ListaAtividades;