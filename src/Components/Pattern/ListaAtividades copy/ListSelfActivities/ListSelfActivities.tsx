import React from "react";
import ItemListSelfActivities from "../ItemListSelfActivities/ItemListSelfActivities";
import { ListSelfActivitiesComp } from "./ListSelfActivitiesComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    cursor?:any;
    className?:any;
    zindex?:string;
    atividades:any
}

const ListSelfActivities:React.FC<Props>=(props)=>{
        return(
            <ListSelfActivitiesComp id={props.id} {...props}>
                {props.atividades.map((atividade:any, index:number)=>(<ItemListSelfActivities key={index} atividade={atividade}/>))}
            </ListSelfActivitiesComp>
        )
}

export default ListSelfActivities;