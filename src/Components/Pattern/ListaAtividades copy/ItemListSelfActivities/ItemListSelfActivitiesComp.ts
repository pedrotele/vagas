import styled from "styled-components";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    flexGrow?:string;
}

export const ItemListSelfActivitiesComp=styled.div<Props>`
    width:${({width}) => width};
    height:${props => props.height};
    background: ${props => props.background || "none"};
    padding:${props => props.padding || 0};
    padding-bottom:${props => props.paddingBottom};
    margin:${props => props.margin || 0};
    display:${props => props.display || "block"};
    float:${props => props.float || "none"};
    top:${props => props.top};
    bottom:${props => props.bottom};
    left:${props => props.left};
    right:${props => props.right};
    position:${props => props.position || "none"};
    box-shadow:${props => props.boxShadow || "none"};
    border-radius:${props => props.borderRadius || "none"};
    justify-content:${props => props.justifyContent || "none"};
    align-itens:${props => props.alignItens || "none"};
    flex-direction:${props => props.flexDirection || "none"};
    clear:${props => props.clear || "none"};
    transform:${props => props.transform || "none"};
    margin-left:${props => props.marginLeft || "none"};
    margin-right:${props => props.marginRight || "none"};
    margin-bottom:${props => props.marginBottom || "none"};
    margin-top:${props => props.marginTop || "none"};
    border:${props => props.border || "none"};
    border-bottom:${props => props.borderBottom};
    border-right:${props => props.borderRight};
    border-top:${props => props.borderTop};
    border-left:${props => props.borderLeft};
    padding-left:${props => props.paddingLeft || "none"};
    border-bottom-right-radius:${props => props.borderBottomRightRadius};
    border-bottom-left-radius:${props => props.borderBottomLeftRadius};
    border-top-right-radius:${props => props.borderTopRightRadius};
    border-top-left-radius:${props => props.borderTopLeftRadius};
    overflow-x:hidden;
    overflow-y:${props => props.overflow||"hidden"};
    flex-grow:${props => props.flexGrow||"hidden"};
    

    /* width */
    ::-webkit-scrollbar {
    width: 10px;
    height:10px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
    background: #f1f1f1;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
    background: #888;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
    background: #555;
    }
`;