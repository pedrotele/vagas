import { Close, Done } from "@material-ui/icons";
import React, { useState } from "react";
import Div from "../../Basics/Div/Div";
import Text from "../../Basics/Text/Text";
import { ConfirmationComp } from "./ConfirmationComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    cursor?:any;
    className?:any;
    zindex?:string;
    flexFlow?:string;
    flexBasis?:string;
    opacity?:string;
    text:string;
    exec:any;
    what:string;
    setDisplay:any;
}

const Confirmation:React.FC<Props>=(props)=>{

    return(
        <ConfirmationComp id={props.id} {...props}>

            <Div display={props.display} zindex="1" position="fixed" top="0" right="0" opacity=".6" height="100vh" width="100vw" background="#999999"/>
    
            <Div display={props.display==="fixed"?"block":"none"} zindex="2" background="white" top="50%" left="50%" transform="translate(-50%, -50%)" boxShadow="1px 1px 1px 1px #999999" borderRadius="10px" border="1px solid #292929" width="40%" position="absolute">
                                    <Text fontSize="75%" padding="10px" text={props.text}/>
                                    <Text fontSize="85%" padding="10px" text={props.what} paddingTop="0px"/>
                                    <Div display="flex" width="95%" margin="0 auto" alignItens="right" justifyContent="right" position="relative">
                                        <Div display="flex" margin="2%" boxShadow="1px 1px 1px 1px #dddddd" borderRadius="5px" onClick={()=>{props.exec()}} padding="10px">
                                            <Div width="25px" height="25px" position="relative" top="50%" transform="translate(0%, -50%)">
                                                <Done htmlColor="green"/>
                                            </Div>
                                            <Text textFillColor="green" position="relative" height="17px" top="50%" transform="translate(0%, -50%)" text="Confirmar" fontSize="80%"/>
                                        </Div>
                                        <Div display="flex" margin="2%" boxShadow="1px 1px 1px 1px #dddddd"  borderRadius="5px" onClick={()=>{props.setDisplay("none")}} padding="10px">
                                            <Div width="25px" height="25px" position="relative" top="50%" transform="translate(0%, -50%)">
                                                <Close htmlColor="red"/>
                                            </Div>
                                            <Text textFillColor="red" position="relative" height="17px" top="50%" transform="translate(0%, -50%)" text="Cancelar" fontSize="80%"/>
                                        </Div>
                                    </Div>
                            </Div>
        </ConfirmationComp>
    )
}

export default Confirmation;