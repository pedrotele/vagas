import React, { useState } from "react";
import  ItemMenu  from "../ItemMenu/ItemMenu";
import { MenuComp } from "./MenuComp";
import Home from '@material-ui/icons/Home'
import Analytics from '@material-ui/icons/AssessmentOutlined'
import {Link, Navigate } from "react-router-dom";
import { Add, Clear ,WhatshotSharp, AccountCircleOutlined, GroupOutlined, Settings, SettingsOutlined } from "@material-ui/icons";
import Div from "../../../Basics/Div/Div";


interface MLProps{
    state:number;
    position?:string;
    marginTop?:string;
    onMouseEnter?:()=>void;
    onMouseLeave?:()=>void;
    width?:string;
    height?:string;
    background?:string;
    borderRight?:string;
}



const Menu:React.FC<MLProps>=(props)=>{

    const [token,setToken]=useState(getCookies()!.substring(1,getCookies()?.length!-1));
    const [user, setUser]=useState(parseJwt(token));


    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }
    
    function parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };


        if(user.is_admin){
            return(
                <MenuComp {...props}>

                    <Div width="100%" top="50%" position="relative" transform="translate(0, -50%)">
                        <Link to="/solicitacoes" style={{color:"#000", textDecoration:"none"}}>
                            <ItemMenu state={props.state} icone={<WhatshotSharp/>} texto="Solicitações" width="100%" height="50px" display="flex"/>
                        </Link>
                        <Link to="/vagas" style={{color:"#000", textDecoration:"none"}}>
                            <ItemMenu state={props.state} icone={<Add/>} texto="Vagas" width="100%" height="50px" display="flex"/>
                        </Link>
                        <Link to="/usuarios" style={{color:"#000", textDecoration:"none"}}>
                            <ItemMenu state={props.state} icone={<GroupOutlined/>} texto="Usuários" width="100%" height="50px" display="flex"/>
                        </Link>
                        <Link to="/perfil" style={{color:"#000", textDecoration:"none"}}>
                            <ItemMenu state={props.state} icone={<AccountCircleOutlined/>} texto="Meu Perfil" width="100%" height="50px" display="flex"/>
                        </Link>
                        <Link to="/settings" style={{color:"#000", textDecoration:"none"}}>
                            <ItemMenu state={props.state} icone={<SettingsOutlined/>} texto="Configurações" width="100%" height="50px" display="flex"/>
                        </Link>
                    </Div>
                </MenuComp>
            )
        }else{
            return(
                <MenuComp {...props}>

                    <Div width="100%" top="50%" position="relative" transform="translate(0, -50%)">
                        <Link to="/vagas" style={{color:"#000", textDecoration:"none"}}>
                            <ItemMenu state={props.state} icone={<Add/>} texto="Vagas" width="100%" height="50px" display="flex"/>
                        </Link>
                        <Link to="/perfil" style={{color:"#000", textDecoration:"none"}}>
                            <ItemMenu state={props.state} icone={<AccountCircleOutlined/>} texto="Meu Perfil" width="100%" height="50px" display="flex"/>
                        </Link>
                        <Link to="/settings" style={{color:"#000", textDecoration:"none"}}>
                            <ItemMenu state={props.state} icone={<SettingsOutlined/>} texto="Configurações" width="100%" height="50px" display="flex"/>
                        </Link>
                    </Div>

                </MenuComp>
            )
        }
}

export default Menu;