import React, { useEffect, useState } from "react";
import Div from "../../../Basics/Div/Div";
import Text from "../../../Basics/Text/Text";
import { ItemMenuComp } from "./ItemMenuComp";


interface ItemMLProps{
    icone:any;
    state:number;
    texto:string;
    width:string;
    height:string;
    display:string;
    marginTop?:string;
    onClick?:any;
}

const ItemMenuLateral:React.FC<ItemMLProps>=(props)=>{

    const [displayText, setDisplayText]=useState("block");

    useEffect(()=>{
        if(props.state==1){
            setDisplayText("flex");
        }else{
            setDisplayText("none");
        }
    },[props.state]);

    return(
        <ItemMenuComp {...props}>
            <Div height="24px" left="18px" position="relative" top="50%" transform="translateY(-50%)">
                {props.icone}
            </Div>
            <Text height="20px" margin="0" padding="0" position="relative" top="50%" transform="translateY(-50%)" fontSize="10pt" left="30px" display={displayText} text={props.texto}/>
        </ItemMenuComp>
    )
}

export default ItemMenuLateral;