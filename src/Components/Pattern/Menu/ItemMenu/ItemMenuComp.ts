import styled from "styled-components";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
}


export const ItemMenuComp=styled.div<Props>`
    width:${props => props.width};
    height:${props => props.height};
    background: ${props => props.background || "none"};
    padding:${props => props.padding || 0};
    margin:${props => props.margin || 0};
    display:${props => props.display || "block"};
    float:${props => props.float || "none"};
    top:${props => props.top || 0};
    bottom:${props => props.bottom || 0};
    left:${props => props.left || 0};
    right:${props => props.right || 0};
    position:${props => props.position || "none"};
    box-shadow:${props => props.boxShadow || "none"};
    border-radius:${props => props.borderRadius || "none"};
    justify-content:${props => props.justifyContent || "none"};
    align-itens:${props => props.alignItens || "none"};
    flex-direction:${props => props.flexDirection || "none"};
    clear:${props => props.clear || "none"};
    transform:${props => props.transform || "none"};
    margin-left:${props => props.marginLeft || "none"};
    margin-right:${props => props.marginRight || "none"};
    margin-bottom:${props => props.marginBottom || "none"};
    margin-top:${props => props.marginTop || "none"};
    border:${props => props.border || "none"};
    border-bottom:${props => props.borderBottom || "none"};
    border-right:${props => props.borderRight || "none"};
    border-top:${props => props.borderTop || "none"};
    border-left:${props => props.borderLeft || "none"};
    padding-left:${props => props.paddingLeft || "none"};
    border-bottom-right-radius:${props => props.borderBottomRightRadius || "none"};
    border-bottom-left-radius:${props => props.borderBottomLeftRadius || "none"};
    border-top-right-radius:${props => props.borderTopRightRadius || "none"};
    border-top-left-radius:${props => props.borderTopLeftRadius || "none"};
    &:hover{
        background:#f5f5f5;
        cursor:pointer;
    };
`;