import React, { useEffect, useState } from "react";
import Home from '@material-ui/icons/Home'
import People from '@material-ui/icons/People'
import Analytics from '@material-ui/icons/AssessmentOutlined'
import {Link, Navigate } from "react-router-dom";
import { Add, AllOut, CalendarToday, Close, Group, Map, Notifications, ShoppingCart, ViewModule, WhatshotSharp } from "@material-ui/icons";
import Image from "../../Basics/Image/Image";
import Div from "../../Basics/Div/Div";
import Text from "../../Basics/Text/Text";


interface MLProps{
    position?:string;
    marginTop?:string;
    onMouseEnter?:()=>void;
    onMouseLeave?:()=>void;
    width?:string;
    height?:string;
    background?:string;
    borderRight?:string;
}

const Header:React.FC<MLProps>=(props)=>{

    return(
        <Div background="#292929" display="flex" flexGrow="0" flexBasis="auto" width="100%" height="45px">

                    <Image image={require("../../../Res/Images/logo.png")} height="25px" width="25px" top="50%" transform="translate(0, -50%)" position="relative"  marginLeft="10px"/>
                    <Image image={require("../../../Res/Images/logoescrito.png")} height="15px" top="50%" transform="translate(0, -50%)" position="relative"  marginLeft="10px"/>
                    <Div width="30%" alignItens="right" justifyContent="right" flexGrow="1" marginRight="10px" float="right" height="100%" display="flex" position="relative"  top="50%" transform="translate(0, -50%)" >
                        <Div width="30px" height="30px" marginLeft="10px" marginRight="10px" borderRadius="50px" border="1px solid #eeeeee" position="relative"  top="50%" transform="translate(0, -50%)" background={require("../../../Res/Images/markTeleguardian.png")}/>
                    </Div>
        </Div>
    )
}

export default Header;