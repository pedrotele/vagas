import React from "react";
import VagaFinal from "../../../../Objects/RH/Vagas/VagaFinalizada";
import Div from "../../../Basics/Div/Div";
import Text from "../../../Basics/Text/Text";
import CelulaVagas from "../CelulaVagas/CelulaVagas";
import { ListaVagHisComp } from "./ListaVagHisComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    cursor?:any;
    className?:any;
    zindex?:string;
    vagas:any;
    historico:any;
    displayType?:any;
}

const ListaVagHis:React.FC<Props>=(props)=>{
    

    if(props.displayType=="historico"){
        return(
            <ListaVagHisComp id={props.id} {...props}>
                <Div borderBottom="1px solid #eeeeee" background="#eeeeee" width="90%" margin="0 auto" height="50px" display={"block"}>

                        <Div top="50%" float="left" transform="translate(0, -50%)"   position="relative" borderRadius="20px" marginLeft="2%" border="1px solid #eeeeee" boxShadow="1px 1px 1px 1px #eeeeee" width="12px" height="12px"/>
                        <Text text={"Nome"} top="50%" float="left"  transform="translate(0, -50%)" position="relative" fontSize="80%" padding="10px" width="20%" overflow="hidden" height="20px"/>
                        <Text text={"Abertura"} float="left" top="50%"  transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="10%" overflow="hidden" height="20px"/>
                        <Text text={"Final do Prazo"} float="left" top="50%"  transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="10%" overflow="hidden" height="20px"/>
                        <Text text={"Data de Fechamento"} float="left" top="50%"  transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="15%" overflow="hidden" height="20px"/>
                        <Text text={"Status"} float="left" top="50%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="25%" overflow="hidden" height="20px"/>
                        
                </Div>
                <Div width="90%" margin="0 auto">
                    {props.vagas.map((vagas:any, index:number)=>(<CelulaVagas  key={index} type={props.displayType} vaga={vagas}/>))}
                </Div>
            </ListaVagHisComp>
        )
    }else{
        return(
            <ListaVagHisComp id={props.id} {...props}>
                <Div borderBottom="1px solid #eeeeee" background="#eeeeee" width="90%" margin="0 auto" height="50px" display={"block"}>
                    
                        <Div top="50%" float="left" transform="translate(0, -50%)"   position="relative" borderRadius="20px" marginLeft="2%" border="1px solid #eeeeee" boxShadow="1px 1px 1px 1px #eeeeee" width="12px" height="12px"/>
                        <Text text={"Nome"} top="50%" float="left" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="80%" padding="10px" width="30%" overflow="hidden" height="20px"/>
                        <Text text={"Abertura"} float="left" top="50%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="7%" overflow="hidden" height="20px"/>
                        <Text text={"Final do Prazo"} float="left" top="50%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="10%" overflow="hidden" height="20px"/>
                        <Text text={"Status"} float="left" top="50%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="20%" overflow="hidden" height="20px"/>
                        
                </Div>
                <Div width="90%" margin="0 auto">
                    {props.vagas.map((vagas:any, index:number)=>(<CelulaVagas key={index}  type={props.displayType} vaga={vagas}/>))}
                </Div>
            </ListaVagHisComp>
        )
    }
}

export default ListaVagHis;