import { Cancel, Clear, Close, Done, Work } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import Data from "../../../../Objects/Agenda/Data";
import VagaFinal from "../../../../Objects/RH/Vagas/VagaFinalizada";
import Div from "../../../Basics/Div/Div";
import Text from "../../../Basics/Text/Text";
import Confirmation from "../../../Pattern/Confirmation/Confirmation";
import { CelulaVagasComp } from "./CelulaVagasComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    vaga:any;
    type:string;
}

const CelulaVagas:React.FC<Props>=(props)=>{

    const start=new Date(props.vaga.created_at);
    const until=new Date(props.vaga.until_at);
    const end=new Date(props.vaga.end_at);
    const [displayConf, setDisplayConf]=useState("none");
    const [stateDisplayConf, setStateDisplayConf]=useState("");

    const [token,setToken]=useState(getCookies()!.substring(1,getCookies()?.length!-1));
    const [user, setUser]=useState({User:{is_admin:false}});
    

    useEffect(()=>{
        getSelf();
    },[])

    async function getSelf(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token}
            };
            await fetch("http://10.1.222.20:3001/persons/self", requestOptions)
                .then(res=>res.json())
                .then(res => {
                    if(res){
                        console.log(res);
                        setUser(res);
                    }
                });
    }


    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }
    
    function parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };


    async function finalizarVaga(){
        const requestOptions = {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token},
            };
            await fetch('http://10.1.222.20:3001/vacancies/'+props.vaga.id, requestOptions)
                .then(res => {window.location.reload()});
    }

    async function cancelarVaga(){
        const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token},
            };
            await fetch('http://10.1.222.20:3001/vacancies/'+props.vaga.id, requestOptions)
                .then(res => {window.location.reload()});
    }


    function getDif(){
        const now=new Date();
        var dif:number=now.getTime()-until.getTime();
        dif=Math.trunc(dif/(1000 * 3600 * 24));
        if(dif<0){
            return (dif*(-1));
        }
        return dif;
    }

    function getTimeToFech(){
        var dif:number=end.getTime()-until.getTime();
        dif=Math.trunc(dif/(1000 * 3600 * 24));
        if(dif<0){
            return (dif*(-1));
        }
        return dif;
    }
    

    function definirTexto(){
        if(props.vaga.is_active){
            if(end.getTime()>until.getTime()){
                if(getTimeToFech()===1){
                    return "Contratado Fora do Prazo - "+getTimeToFech()+" Dia de Atraso";
                }else{
                    return "Contratado Fora do Prazo - "+getTimeToFech()+" Dias de Atraso";
                }
            }else{
                if(getTimeToFech()===1){
                    return "Contratado Dentro do Prazo - "+getDif()+" Dia";
                }else{
                    return "Contratado Dentro do Prazo - "+getDif()+" Dias";
                }
            }
        }else{
            return "Cancelada";
        }
    }
    

    if(props.type==="historico"){
        return(
            <CelulaVagasComp float="left" borderBottom="1px solid #eeeeee" background="white" width="100%" height="50px" display="block" margin="0 auto">
                    <Div top="50%" float="left" transform="translate(0, -50%)"   position="relative" background={end.getTime()>=until.getTime()?"red":"green"} borderRadius="20px" marginLeft="2%" border="1px solid #eeeeee" boxShadow="1px 1px 1px 1px #eeeeee" width="12px" height="12px"/>
                    <Text text={props.vaga.name} top="50%" whiteSpace="nowrap" textOverflow="ellipsis" float="left" transform="translate(0, -50%)" position="relative" fontSize="80%" padding="10px" width="20%" overflow="hidden" height="20px"/>
                    <Text text={start.getDate()+"/"+(start.getMonth()+1)+"/"+start.getFullYear()} float="left" top="50%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="10%" overflow="hidden" height="20px"/>
                    <Text text={until.getDate()+"/"+(until.getMonth()+1)+"/"+until.getFullYear()} float="left" top="50%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="10%" overflow="hidden" height="20px"/>
                    <Text text={end.getDate()+"/"+(end.getMonth()+1)+"/"+end.getFullYear()} float="left" top="50%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="15%" overflow="hidden" height="20px"/>
                    <Text text={definirTexto()} textFillColor={props.vaga.is_active?(end.getTime()>=until.getTime()?"red":"green"):("red")} float="left" top="50%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="70%" textAlign="center" padding="10px" width="25%" overflow="hidden" height="20px"/>
        

            </CelulaVagasComp>
        )}else{
            if(user.User.is_admin){

                return(
                    <CelulaVagasComp borderBottom="1px solid #eeeeee" background="white" width="100%" margin="0 auto" height="50px" display="block">
                    
                            <Div top="50%" float="left" transform="translate(0, -50%)"   position="relative" background={until.getTime()>=new Date().getTime()?"green":"red"} borderRadius="20px" marginLeft="2%" border="1px solid #eeeeee" boxShadow="1px 1px 1px 1px #eeeeee" width="12px" height="12px"/>
                            <Text text={props.vaga.name} top="50%" float="left" whiteSpace="nowrap" textOverflow="ellipsis" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="75%" padding="10px" width="30%" overflow="hidden" height="20px"/>
                            <Text text={start.getDate()+"/"+(start.getMonth()+1)+"/"+start.getFullYear()} float="left" top="50%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="7%" overflow="hidden" height="20px"/>
                            <Text text={until.getDate()+"/"+(until.getMonth()+1)+"/"+until.getFullYear()} float="left" top="50%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="10%" overflow="hidden" height="20px"/>
                            <Text text={(until.getTime()>=new Date().getTime()?"No Prazo":"Atrasada")+" - "+getDif()+(getDif()!==1?" dias restantes":" dia restante")} textFillColor={until.getTime()>=new Date().getTime()?"green":"red"} float="left" top="50%" width="25%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="78%" textAlign="center" height="20px"/>
                            <Div onClick={()=>{setDisplayConf("fixed"); setStateDisplayConf("work")}} margin="0" float="left" padding="10px" height="25px" width="15px" top="50%" transform="translate(0, -50%)" position="relative">
                                <Work/>
                            </Div>
                            <Div onClick={()=>{setDisplayConf("fixed"); setStateDisplayConf("cancel")}} margin="0" float="left" padding="10px" height="25px" width="15px" top="50%" transform="translate(0, -50%)" position="relative">
                                <Clear/>
                            </Div>
    
                            <Confirmation setDisplay={setDisplayConf} display={displayConf} what={props.vaga.name} exec={stateDisplayConf==="work"?finalizarVaga:cancelarVaga}  text={stateDisplayConf==="work"?"Você realmente deseja solicitar a confirmação da contratação de um funcionário para a vaga:":"Você realmente deseja cancelar a vaga:"}/>
                            
    
                    </CelulaVagasComp>
                )

            }else{

                return(

                    <CelulaVagasComp borderBottom="1px solid #eeeeee" background="white" width="100%" margin="0 auto" height="50px" display="block">
                    
                            <Div top="50%" float="left" transform="translate(0, -50%)"   position="relative" background={until.getTime()>=new Date().getTime()?"green":"red"} borderRadius="20px" marginLeft="2%" border="1px solid #eeeeee" boxShadow="1px 1px 1px 1px #eeeeee" width="12px" height="12px"/>
                            <Text text={props.vaga.name} top="50%" float="left" whiteSpace="nowrap" textOverflow="ellipsis" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="80%" padding="10px" width="25%" overflow="hidden" height="20px"/>
                            <Text text={start.getDate()+"/"+(start.getMonth()+1)+"/"+start.getFullYear()} float="left" top="50%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="7%" overflow="hidden" height="20px"/>
                            <Text text={until.getDate()+"/"+(until.getMonth()+1)+"/"+until.getFullYear()} float="left" top="50%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="80%" textAlign="center" padding="10px" width="10%" overflow="hidden" height="20px"/>
                            <Text text={(until.getTime()>=new Date().getTime()?"No Prazo":"Atrasada")+" - "+getDif()+(getDif()!==1?" dias restantes":" dia restante")} textFillColor={until.getTime()>=new Date().getTime()?"green":"red"} float="left" top="50%" width="30%" marginLeft="2%" transform="translate(0, -50%)" position="relative" fontSize="70%" textAlign="center" height="20px"/>
                            <Div onClick={()=>{setDisplayConf("fixed"); setStateDisplayConf("work")}} margin="0" float="left" padding="10px" height="25px" width="15px" top="50%" transform="translate(0, -50%)" position="relative">
                                <Work/>
                            </Div>
    
    
                            <Confirmation setDisplay={setDisplayConf} display={displayConf} what={props.vaga.name} exec={stateDisplayConf==="work"?finalizarVaga:cancelarVaga}  text={stateDisplayConf==="work"?"Você realmente deseja solicitar a confirmação da contratação de um funcionário para a vaga:":"Você realmente deseja cancelar a vaga:"}/>
    
                    </CelulaVagasComp>
                )

            }
    }
}

export default CelulaVagas;