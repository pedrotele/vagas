import { CallOutlined, FaceOutlined } from "@material-ui/icons";
import React, { useEffect, useState } from "react";
import Div from "../../Basics/Div/Div";
import Text from "../../Basics/Text/Text";
import Button from "../../Forms/Button/Button";
import DropDown from "../../Forms/DropDown/DropDown/DropDown";
import Input from "../../Forms/Input/Input";
import ListaContatos from "../Contatos/ListaContatos/ListaContatos";
import { AccountFormsComp } from "./AccountFormsComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    cursor?:any;
    className?:any;
    zindex?:string;
    flexFlow?:string;
    flexBasis?:string;
    opacity?:string;
    withPassword:boolean;
}

const AccountForms:React.FC<Props>=(props)=>{

    const [email, setEmail]=useState("");
    const [cpf, setCpf]=useState("");
    const [senha, setSenha]=useState("");
    const [name, setName]=useState("");
    const [stateDropdown, setStateDropDown]=useState("none");
    const [selectedType, setSelectedType]=useState("");
    const [password, setPassword]=useState("");
    const [contatos, setContatos]=useState(new Array);
    const [newContato, setNewContato]=useState("");

    const [token,setToken]=useState(getCookies()!.substring(1,getCookies()?.length!-1));
    const [userToken, setUserToken]=useState(parseJwt(token));
    const [user, setUser]=useState<any>();


    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }

    useEffect(()=>{
        buscarUser();
    },[]);
    
    function parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
        return JSON.parse(jsonPayload);
    };

    async function buscarUser(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token}
            };
            await fetch("http://10.1.222.20:3001/persons/self", requestOptions)
                .then(res=>res.json())
                .then(res => {
                    if(res){
                        setUser(res);
                        setName(res.name);
                        setCpf(res.cpf);
                        setEmail(res.email);
                        setContatos(res.phones);
                    }
                });
    }



    function adicionarContato(){
        if(!contatos.includes(newContato)){
            setContatos([...contatos, newContato]);
            setNewContato("");
        }
    }

    function excluirContato(e:string){
        if(contatos.includes(e)){
            const cont=contatos.filter(conta=>conta!==e);
            setContatos(cont);
        }
    }

    async function cadastrarUsuario(){
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token},
            body: JSON.stringify({name, email, phones:contatos, cpf, User:{create:{is_admin:selectedType==="Admin"?true:false, password:senha}}})
            };
            await fetch("http://10.1.222.20:3001/persons", requestOptions)
                .then(res => {window.location.reload()});
    }

    async function alterarUsuario(){
        const requestOptions = {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token},
            body: JSON.stringify({name, email, phones:contatos, cpf})
            };
            await fetch("http://10.1.222.20:3001/persons/"+user.id, requestOptions)
                .then(res => {window.location.reload()});
    }
       
    if(props.withPassword){
        return(
            <Div  marginLeft="50px" flexGrow="1" height="100%" marginRight="20%" display="flex">
                <Div width="100%" height="100%" display="block" overflow="scroll">
                                <Text width="100%" fontSize="70%" text="Página atual:" margin="30px" marginBottom="5px" marginLeft="70px"/>
                                <Text width="100%" fontSize="150%" text="Adicionar Usuário" margin="30px" marginTop="0px" marginLeft="70px"/>
                                <Div marginTop="5%" width="90%" display="flex" margin="0 auto">
                                    <Div width="60%" display="block">
                                        <Div display="flex" height="35px" marginBottom="20px">
                                            <Div width="35px" height="35px">
                                                <FaceOutlined style={{height:"35px", width:"35px"}}/>
                                            </Div>
                                            <Text text="Informações Pessoais" position="relative" top="50%" transform="translate(0, -50%)" fontSize="100%" height="25px" marginLeft="15px"/>
                                        </Div>
                                        <Div display="flex" height="40px" marginBottom="10px">
                                            <Text width="15%" text="Nome:" height="25px" fontSize="100%" position="relative" top="50%" transform="translate(0, -50%)" />
                                            <Input background="#ffffff" marginLeft="10px" onChange={(e)=>{setName(e.target.value)}} width="100%" margin="0 auto" padding="10px" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" placeholder="Nome" />
                                        </Div>
                                        <Div display="flex" height="40px" marginBottom="10px">
                                            <Text width="15%" text="CPF:" height="25px" fontSize="100%" position="relative" top="50%" transform="translate(0, -50%)" />
                                            <Input background="#ffffff" marginLeft="10px" onChange={(e)=>{setCpf(e.target.value)}} width="100%" margin="0 auto" padding="10px" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" placeholder="CPF" />
                                        </Div>
                                        <Div display="flex" height="40px" marginBottom="10px">
                                            <Text width="15%" text="Email:" height="25px" fontSize="100%" position="relative" top="50%" transform="translate(0, -50%)" />
                                            <Input background="#ffffff" marginLeft="10px" onChange={(e)=>{setEmail(e.target.value)}} width="100%" margin="0 auto" padding="10px" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" placeholder="Email" />
                                        </Div>
                                        <Div display="flex" height="40px" marginBottom="10px">
                                            <Text width="15%" text="Senha:" height="25px" fontSize="100%" position="relative" top="50%" transform="translate(0, -50%)" />
                                            <Input background="#ffffff" marginLeft="10px" onChange={(e)=>{setPassword(e.target.value)}} width="100%" margin="0 auto" padding="10px" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" placeholder="Senha" />
                                        </Div>
                                        <DropDown state={stateDropdown} marginBottom="10px" setState={setStateDropDown} width="100%" margin="0 auto"  selected={selectedType} setSelected={setSelectedType} background="white" itens={["Admin","Usuário"]} />
                                    </Div>
                                    <Div margin="2%" width="3px" marginTop="8%" height="200px" background="#292929"/>
                                    <Div width="35%" display="block">
                                        <Div display="flex" height="35px" marginBottom="20px">
                                            <Div width="35px" height="35px">
                                                <CallOutlined style={{height:"35px", width:"35px"}}/>
                                            </Div>
                                            <Text text="Telefones" position="relative" top="50%" transform="translate(0, -50%)" fontSize="100%" height="25px" marginLeft="15px"/>
                                        </Div>
                                        <Div width="100%" display="block">
                                            <Input background="#ffffff" value={newContato} onChange={(e)=>{setNewContato(e.target.value)}} width="100%" padding="10px" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" placeholder="Número"/>
                                            <Div  width="100%" margin="0 auto" borderBottom="none" marginTop="5px" onClick={()=>{adicionarContato()}}>
                                                <Button width="100%" margin="0 auto" padding="10px"  borderBottom="none" border="none" borderRadius="5px" color="#ffffff" background="#292929">Adicionar Número</Button>
                                            </Div>
                                        </Div>
                                        <ListaContatos excluirContato={excluirContato} marginTop="10px" lista={contatos}/>
                                    </Div>
                                </Div>
                                <Div width="90%" margin="0 auto" borderBottom="none" marginTop="4%" onClick={()=>{cadastrarUsuario()}}>
                                    <Button width="100%" margin="0 auto" padding="10px" borderBottom="none" border="none" borderRadius="5px" color="#ffffff" background="#292929">Cadastrar</Button>
                                </Div>
                            </Div>
                </Div>
        )
    }else{
        return(
            <AccountFormsComp id={props.id} {...props}>
                                <Div marginTop="5%" width="90%" display="flex" margin="0 auto">
                                    <Div width="60%" display="block">
                                        <Div display="flex" height="35px" marginBottom="20px">
                                            <Div width="35px" height="35px">
                                                <FaceOutlined style={{height:"35px", width:"35px"}}/>
                                            </Div>
                                            <Text text="Informações Pessoais" position="relative" top="50%" transform="translate(0, -50%)" fontSize="100%" height="25px" marginLeft="15px"/>
                                        </Div>
                                        <Div display="flex" height="40px" marginBottom="10px">
                                            <Text width="15%" text="Nome:" height="25px" fontSize="100%" position="relative" top="50%" transform="translate(0, -50%)" />
                                            <Input background="#ffffff" marginLeft="10px" value={name} onChange={(e)=>{setName(e.target.value)}} width="100%" margin="0 auto" padding="10px" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" placeholder="Nome" />
                                        </Div>
                                        <Div display="flex" height="40px" marginBottom="10px">
                                            <Text width="15%" text="CPF:" height="25px" fontSize="100%" position="relative" top="50%" transform="translate(0, -50%)" />
                                            <Input background="#ffffff" marginLeft="10px" value={cpf} onChange={(e)=>{setCpf(e.target.value)}} width="100%" margin="0 auto" padding="10px" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" placeholder="CPF" />
                                        </Div>
                                        <Div display="flex" height="40px" marginBottom="10px">
                                            <Text width="15%" text="Email:" height="25px" fontSize="100%" position="relative" top="50%" transform="translate(0, -50%)" />
                                            <Input background="#ffffff" marginLeft="10px" value={email} onChange={(e)=>{setEmail(e.target.value)}} width="100%" margin="0 auto" padding="10px" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" placeholder="Email" />
                                        </Div>
                                    </Div>
                                    <Div margin="2%" width="3px" marginTop="8%" height="200px" background="#292929"/>
                                    <Div width="35%" display="block">
                                        <Div display="flex" height="35px" marginBottom="20px">
                                            <Div width="35px" height="35px">
                                                <CallOutlined style={{height:"35px", width:"35px"}}/>
                                            </Div>
                                            <Text text="Telefones" position="relative" top="50%" transform="translate(0, -50%)" fontSize="100%" height="25px" marginLeft="15px"/>
                                        </Div>
                                        <Div width="100%" display="block">
                                            <Input background="#ffffff" value={newContato} onChange={(e)=>{setNewContato(e.target.value)}} width="100%" padding="10px" fontSize="10pt" display="block" borderBottom="3px solid #eeeeee" placeholder="Número"/>
                                            <Div  width="100%" margin="0 auto" borderBottom="none" marginTop="5px" onClick={()=>{adicionarContato()}}>
                                                <Button width="100%" margin="0 auto" padding="10px"  borderBottom="none" border="none" borderRadius="5px" color="#ffffff" background="#292929">Adicionar Número</Button>
                                            </Div>
                                        </Div>
                                        <ListaContatos excluirContato={excluirContato} marginTop="10px" lista={contatos}/>
                                    </Div>
                                </Div>
                                <Div width="90%" margin="0 auto" display={(user!=undefined)?((name!==user.name)||(email!==user.email)||(cpf!==user.cpf)||(contatos!==user.phones)?"block":"none"):"none"} borderBottom="none" marginTop="4%" onClick={()=>{alterarUsuario()}}>
                                            <Button width="100%" margin="0 auto" padding="10px" borderBottom="none" border="none" borderRadius="5px" color="#ffffff" background="#292929">Confirmar Alterações</Button>
                                </Div>
            </AccountFormsComp>
        )
    }
}

export default AccountForms;