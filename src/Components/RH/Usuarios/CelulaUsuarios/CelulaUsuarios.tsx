import { Cancel, Clear, Close, Done, Work } from "@material-ui/icons";
import React, { useState } from "react";
import Data from "../../../../Objects/Agenda/Data";
import Setor from "../../../../Objects/Primarios/Setor";
import VagaFinal from "../../../../Objects/RH/Vagas/VagaFinalizada";
import Div from "../../../Basics/Div/Div";
import Image from "../../../Basics/Image/Image";
import Text from "../../../Basics/Text/Text";
import { CelulaUsuariosComp } from "./CelulaUsuariosComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    usuario:any;
}

const CelulaUsuarios:React.FC<Props>=(props)=>{

    const [typeChange,setTypeChange]=useState("");

        return(
            <CelulaUsuariosComp minwidth="150px" borderRadius="10px" float="left" margin="1%" borderBottom="1px solid #eeeeee" background="white"  width="31%" height="80px" display="block">
             
                <Text text={props.usuario.name} fontSize="100%" width="80%" margin="0 auto" height="20px" marginTop="20px"/>
                <Text text={props.usuario.cpf} fontSize="70%" width="80%" margin="0 auto" height="15px" textFillColor="#999999"/>

            </CelulaUsuariosComp>
        )
}

export default CelulaUsuarios;