import React from "react";
import { Link } from "react-router-dom";
import Setor from "../../../../Objects/Primarios/Setor";
import VagaFinal from "../../../../Objects/RH/Vagas/VagaFinalizada";
import Div from "../../../Basics/Div/Div";
import Text from "../../../Basics/Text/Text";
import CelulaUsuarios from "../CelulaUsuarios/CelulaUsuarios";
import { ListaUsuariosComp } from "./ListaUsuariosComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    cursor?:any;
    className?:any;
    zindex?:string;
    usuarios:any;
}

const ListaUsuarios:React.FC<Props>=(props)=>{
        return(
            <ListaUsuariosComp id={props.id} {...props}>
                    {props.usuarios.map((usuario:any, index:number)=>(<CelulaUsuarios key={index} usuario={usuario}/>))}
            </ListaUsuariosComp>
        )
}

export default ListaUsuarios;