import { EmailOutlined } from "@material-ui/icons";
import React, { useState } from "react";
import Div from "../../../Basics/Div/Div";
import Text from "../../../Basics/Text/Text";
import CelulaContatos from "../CelulaContatos/CelulaContatos";
import { ListaContatosComp } from "./ListaContatosComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    cursor?:any;
    className?:any;
    zindex?:string;
    lista:any;
    excluirContato:any;
}

const ListaContatos:React.FC<Props>=(props)=>{

        return(
            <ListaContatosComp id={props.id} {...props}>
                {props.lista.map((contato:any, index:number)=>(<CelulaContatos key={index} excluirContato={props.excluirContato} contato={contato}/>))}
            </ListaContatosComp>
        )
}

export default ListaContatos;