import { Cancel, Clear, Close, DeleteOutline, Done, Work } from "@material-ui/icons";
import React, { useState } from "react";
import Data from "../../../../Objects/Agenda/Data";
import Setor from "../../../../Objects/Primarios/Setor";
import VagaFinal from "../../../../Objects/RH/Vagas/VagaFinalizada";
import Div from "../../../Basics/Div/Div";
import Image from "../../../Basics/Image/Image";
import Text from "../../../Basics/Text/Text";
import { CelulaContatosComp } from "./CelulaContatosComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    contato:any;
    excluirContato:any;
}

const CelulaContatos:React.FC<Props>=(props)=>{
        return(
            <CelulaContatosComp background="white" borderBottom="1px solid #eeeeee" width="100%" height="40px" display="flex">
                <Text text={props.contato} padding="12px" paddingLeft="20px" fontSize="80%" width="75%" />
                <Div height="24px" width="24px" padding="8px" onClick={()=>{props.excluirContato(props.contato)}}>
                    <DeleteOutline/>
                </Div>
            </CelulaContatosComp>
        )
}

export default CelulaContatos;