import React from "react";
import { Link } from "react-router-dom";
import Setor from "../../../../Objects/Primarios/Setor";
import VagaFinal from "../../../../Objects/RH/Vagas/VagaFinalizada";
import Div from "../../../Basics/Div/Div";
import Text from "../../../Basics/Text/Text";
import CelulaSolicitacoes from "../CelulaSolicitacoes/CelulaSolicitacoes";
import { ListaSolicitacoesComp } from "./ListaSolicitacoesComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    cursor?:any;
    className?:any;
    zindex?:string;
    solicitacoes:any;
}

const ListaSolicitacoes:React.FC<Props>=(props)=>{
        return(
            <ListaSolicitacoesComp id={props.id} {...props}>
                <Div width="100%" margin="0 auto">
                    {props.solicitacoes.map((solicitacoes:any, index:number)=>(<CelulaSolicitacoes key={index} solicitacao={solicitacoes}/>))}
                </Div>
            </ListaSolicitacoesComp>
        )
}

export default ListaSolicitacoes;