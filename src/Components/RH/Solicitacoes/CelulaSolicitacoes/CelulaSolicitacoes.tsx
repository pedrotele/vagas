import { Cancel, Clear, Close, Done, Work } from "@material-ui/icons";
import React, { useState } from "react";
import Data from "../../../../Objects/Agenda/Data";
import Setor from "../../../../Objects/Primarios/Setor";
import VagaFinal from "../../../../Objects/RH/Vagas/VagaFinalizada";
import Div from "../../../Basics/Div/Div";
import Image from "../../../Basics/Image/Image";
import Text from "../../../Basics/Text/Text";
import Confirmation from "../../../Pattern/Confirmation/Confirmation";
import { CelulaSolicitacoesComp } from "./CelulaSolicitacoesComp";

interface Props{
    width?: string;
    height?: string;
    background?:string;
    padding?:string;
    paddingBottom?:string;
    margin?:string;
    display?:string;
    float?:string;
    top?:string;
    bottom?:string;
    left?:string;
    right?:string;
    position?:string;
    boxShadow?:string;
    borderRadius?:string;
    justifyContent?:string;
    alignItens?:string;
    flexDirection?:string;
    clear?:string;
    transform?:string,
    marginLeft?:string;
    marginRight?:string;
    marginBottom?:string;
    marginTop?:string;
    border?:string;
    borderBottom?:string;
    borderRight?:string;
    borderTop?:string;
    borderLeft?:string;
    paddingLeft?:string;
    borderBottomRightRadius?:string;
    borderBottomLeftRadius?:string;
    borderTopRightRadius?:string;
    borderTopLeftRadius?:string;
    overflow?:string;
    id?:string;
    flexGrow?:string;
    onClick?:any;
    solicitacao:any;
}

const CelulaSolicitacoes:React.FC<Props>=(props)=>{

    const [token,setToken]=useState(getCookies()!.substring(1,getCookies()?.length!-1));
    const [user, setUser]=useState(parseJwt(token));
    const [typeChange,setTypeChange]=useState("");
    const [created_at,setCreatedAt]=useState(new Date(props.solicitacao.created_at));
    const [until_at,setUntilAt]=useState(new Date(props.solicitacao.until_at));
    const [end_at,setEndAt]=useState(new Date(props.solicitacao.end_at));

    function getCookies(){
        var cookies = " " + document.cookie;
        var key = " " + "token" + "=";
        var start = cookies.indexOf(key);
        if (start === -1) return null;
        var pos = start + key.length;
        var last = cookies.indexOf(";", pos);
        if (last !== -1) {
            return cookies.substring(pos, last);
        }else{
            return cookies.substring(pos);
        }
    }
    
    function parseJwt (token:any) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };

    const [confirmDisplay, setConfirmDisplay]=useState("none");

    function cancelarContratacao(){
        setConfirmDisplay("none");
        setTypeChange("");
    }

    function confirmChange(type:string){
        setTypeChange(type);
        setConfirmDisplay("fixed");
    }

    function getDemora(){
        return Math.floor((end_at.getTime()-created_at.getTime())/(24*60*60*1000)+1);
    }

    async function finalizarVaga(){
        if(typeChange==="approve"){
                const requestOptions = {
                    method: 'PATCH',
                    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token},
                    };
                    await fetch('http://10.1.222.20:3001/vacancies/approve/'+props.solicitacao.id, requestOptions)
                        .then(res => {window.location.reload()});
        }else{
            const requestOptions = {
                method: 'PATCH',
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer '+token},
                };
                await fetch('http://10.1.222.20:3001/vacancies/disapprove/'+props.solicitacao.id, requestOptions)
                    .then(res => {window.location.reload()});
        }
    }

        return(
            <CelulaSolicitacoesComp borderRadius="10px" marginBottom="3%" margin="0 auto" borderBottom="1px solid #eeeeee" background="white" width="90%" display="block" boxShadow="1px 1px 1px 1px #eeeeee">
             
                <Div width="90%" marginTop="1%" margin="0 auto" display="flex" >
                    <Text text={"RH"} marginRight="1%"  marginTop="2%" fontSize="100%"/>
                    <Text text={" - "} marginTop="2%" fontSize="100%"/>
                    <Text text={"Confirmação de Contratação"} marginLeft="1%" marginTop="2%" fontSize="100%"/>
                </Div>
                <Div width="90%" margin="0 auto">
                    <Text text={"Descrição:"} marginTop="2%" fontSize="90%"/>
                    <Div display="flex" marginTop="1%" justifyContent="bottom" alignItens="bottom">
                        <Text text={"Solicito a confirmação da contratação de um funcionário para a vaga de"} textFillColor="#111111" marginTop="5px" height="20px" fontSize="80%"/>
                        <Text text={props.solicitacao.name+"."} marginLeft="1%" fontSize="110%" height="25px" textFillColor="#000000" />
                    </Div>
                    
                </Div>
                <Div width="80%" margin="0 auto" display="flex" >
                    <Div display="block" width="25%" height="100%">
                        <Text text="Abertura" marginLeft="5%" textAlign="center" fontSize="80%"/>
                        <Text text={created_at.getDate()+"/"+created_at.getMonth()+"/"+created_at.getFullYear()} padding="10px" marginLeft="5%" textAlign="center" marginTop="2%" fontSize="80%"/>
                    </Div>
                    <Div display="block" width="25%" height="100%">
                        <Text text="Fechamento" marginLeft="5%" textAlign="center" fontSize="80%"/>
                        <Text text={end_at.getDate()+"/"+end_at.getMonth()+"/"+end_at.getFullYear()} padding="10px" marginLeft="5%" marginTop="2%" textAlign="center" fontSize="80%"/>
                    </Div>
                    <Div display="block" width="25%" height="100%">
                        <Text text="Prazo" marginLeft="5%" textAlign="center" fontSize="80%"/>
                        <Text text={until_at.getDate()+"/"+until_at.getMonth()+"/"+until_at.getFullYear()} padding="10px" marginLeft="5%" textAlign="center" marginTop="2%" fontSize="80%"/>
                    </Div>
                    <Div display="block" width="25%" height="100%">
                        <Text text="Tempo Gasto" marginLeft="5%" textAlign="center" fontSize="80%"/>
                        <Text text={getDemora()+(getDemora()>1?" Dias":" Dia")} padding="10px" marginLeft="5%" textAlign="center" marginTop="2%" fontSize="80%"/>
                    </Div>
                </Div>
                <Div display="flex" width="95%" marginTop="2%" margin="0 auto" alignItens="center" justifyContent="center" position="relative">
                        <Div display="flex" margin="4%" marginBottom="0" boxShadow="1px 1px 1px 1px #dddddd" borderRadius="5px" onClick={()=>{confirmChange("approve")}} padding="10px" position="relative" top="50%" transform="translate(0%, -50%)">
                            <Div width="25px" height="25px" position="relative" top="50%" transform="translate(0%, -50%)">
                                <Done htmlColor="green"/>
                            </Div>
                            <Text textFillColor="green" position="relative" height="17px" top="50%" transform="translate(0%, -50%)" text="Aprovar" fontSize="80%"/>
                        </Div>
                        <Div display="flex" margin="4%" marginBottom="0" borderRadius="5px" boxShadow="1px 1px 1px 1px #dddddd" onClick={()=>{confirmChange("desapprove")}} padding="10px" position="relative" top="50%" transform="translate(0%, -50%)">
                            <Div width="25px" height="25px" position="relative" top="50%" transform="translate(0%, -50%)">
                                <Close htmlColor="red"/>
                            </Div>
                            <Text textFillColor="red" position="relative" height="17px" top="50%" transform="translate(0%, -50%)" text="Negar" fontSize="80%"/>
                        </Div>
                </Div>

                <Confirmation setDisplay={setConfirmDisplay} display={confirmDisplay} what={props.solicitacao.name} exec={typeChange==="approve"?finalizarVaga:cancelarContratacao}  text={typeChange==="approve"?"Você realmente deseja aceitar a contratação de um funcionário para a vaga:":"Você realmente negar a contratação de um funcionário para a vaga:"}/>
                            

            </CelulaSolicitacoesComp>
        )
}

export default CelulaSolicitacoes;